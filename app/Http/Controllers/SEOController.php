<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Brand;
use App\Models\Car;
use App\Models\Story;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Psy\Util\Json;
use DaveChild\TextStatistics as TS;

class SEOController extends Controller
{
    public $response = [];
    public function checkSlug(Request $request) {

        try {
            if($request->input('action')=="brand") {
                $slug = SlugService::createSlug(Brand::class, 'slug', $request->title);
            }elseif ($request->input('action')=="car"){
                $slug = SlugService::createSlug(Car::class, 'slug', $request->title);
            }elseif ($request->input('action')=="blog"){
                $slug = SlugService::createSlug(Blog::class, 'slug', $request->title);
            }elseif ($request->input('action')=="story"){
                $slug = SlugService::createSlug(Story::class, 'slug', $request->title);
            }
            $this->response['code'] = 1;
            $this->response['data'] = $slug;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }

        return Json::encode($this->response);
    }
    public function checkText(Request $request) {
        $data=[];
        $textStatistics = new TS\TextStatistics;
        $text = new TS\Text;
        $data['readability']=$textStatistics->fleschKincaidReadingEase($request->text);
        $data['textLength']=$text->textLength($request->text);
        $data['lettersCount']=$text->letterCount($request->text);
        $data['wordsCount']=$text->wordCount($request->text);
        $data['sentencesCount']=$text->sentenceCount($request->text);
        try {

            $this->response['code'] = 1;
            $this->response['data'] = $data;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }

        return Json::encode($this->response);
    }


}
