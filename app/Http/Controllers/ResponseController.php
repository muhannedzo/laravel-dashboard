<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponseController extends Controller
{

    //
    static public function response($code,$message,$data){
        $response=[];
        $response['code']=$code;
        $response['message']=$message;
        $response['data']=$data;
        return $response;
    }
}
