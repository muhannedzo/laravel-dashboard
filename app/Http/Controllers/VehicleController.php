<?php

namespace App\Http\Controllers;

use App\Models\Body;
use App\Models\Booking;
use App\Models\Brand;
use App\Models\Car;
use App\Models\Type;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Psy\Util\Json;
use Yajra\DataTables\DataTables;

class VehicleController extends Controller
{
    public $response = [];
//    brands
    function getBrandsDetails(Brand $key)
    {

        return view('admin.pages.brands.edit-brand', ['data' => $key, 'page' => 'allBrands']);
    }
    function getBrandsDatatable(Request $request)
    {
        if ($request->ajax()) {
         if (!empty($request->get('search'))) {
                $data = Brand::where('name', 'LIKE', '%' . $request->get('search') . '%');
            } else {
//                $data = User::latest()->get();
                $data = Brand::all();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }
    function postBrand(Request $req)
    {

        $req->validate([
            'img' => 'required|mimes:png,jpg,jpeg,svg|max:100',
            'cover' => 'required|mimes:png,jpg,jpeg,webp|max:200',
            'meta_image' => 'required|mimes:png,jpg,jpeg,webp|max:200',
            'name' => 'required',
            'order_num' => 'required | integer',
            'slug' => 'required | alpha_dash | unique:brands',
            'meta_title' => 'required ',
            'meta_keywords' => 'required ',

        ]);
        try {
            $img = $req->file('img')->store('uploads/brands', 'public');
            $cover = $req->file('cover')->store('uploads/brands', 'public');
            $meta_image = $req->file('meta_image')->store('uploads/brands', 'public');
            $query = Brand::insert([
                'name' => $req->input('name'),
                'img' => $img,
                'cover' => $cover,
                'meta_image' => $meta_image,
                'description' => $req->input('description'),
                'slug' => $req->input('slug'),
                'order_num' => $req->input('order_num'),
                'meta_title' => $req->input('meta_title'),
                'meta_keywords' => $req->input('meta_keywords'),
                'meta_description' => $req->input('meta_description'),
            ]);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function editBrand(Request $req)
    {
        $imgQuery=[];
        $validate=[
            'name' => 'required',
            'order_num' => 'required | integer',
            'slug' => 'required | alpha_dash | unique:brands,slug,'.$req->input('id').'',
            'meta_title' => 'required ',
            'meta_keywords' => 'required ',

        ];
        if ($req->hasFile('img')) {
            $img = $req->file('img')->store('uploads/brands', 'public');
            $imgQuery['img']=$img;
            $validate['img']='required | mimes:png,jpg,jpeg,svg|max:100';
        }
        if ($req->hasFile('cover')) {
            $cover = $req->file('cover')->store('uploads/brands', 'public');
            $imgQuery['cover']=$cover;
            $validate['cover']='required | mimes:png,jpg,jpeg,webp|max:200';
        }
        if ($req->hasFile('meta_image')) {
            $meta_image = $req->file('meta_image')->store('uploads/brands', 'public');
            $imgQuery['meta_image']=$meta_image;
            $validate['meta_image']='required | mimes:png,jpg,jpeg,webp|max:200';
        }
        $req->validate($validate);
        try {
            $sqlQuery=[
                'name' => $req->input('name'),
                'description' => $req->input('description'),
                'slug' => $req->input('slug'),
                'order_num' => $req->input('order_num'),
                'meta_title' => $req->input('meta_title'),
                'meta_keywords' => $req->input('meta_keywords'),
                'meta_description' => $req->input('meta_description'),
            ];
            $sqlQuery=array_merge($sqlQuery,$imgQuery);
//            var_dump($sqlQuery);
//            exit();

            $query = Brand::where(['id' => $req->input('id')])->update($sqlQuery);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function deleteBrand(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Brand::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
//    types
    function getTypesDetails(Type $key)
    {

        return view('admin.pages.types.edit-type', ['data' => $key, 'page' => 'allTypes']);
    }
    function getTypesDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Type::where('name_en', 'LIKE', '%' . $request->get('search') . '%');
            } else {
//                $data = User::latest()->get();
                $data = Type::all();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }
    function postType(Request $req)
    {

        $req->validate([
            'img' => 'required|mimes:png,jpg,jpeg,svg|max:100',
            'nameEn' => 'required | unique:types,name_en',
            'nameAr' => 'required | unique:types,name_ar',
        ]);
        try {
            $img = $req->file('img')->store('uploads/types', 'public');
            $query = Type::insert([
                'name_en' => $req->input('nameEn'),
                'name_ar' => $req->input('nameAr'),
                'img' => $img,
            ]);
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function editType(Request $req)
    {
        $imgQuery=[];
        $validate=[
            'nameEn' => 'required |  unique:types,name_en,'.$req->input('id').'',
            'nameAr' => 'required |  unique:types,name_ar,'.$req->input('id').'',
        ];
        if ($req->hasFile('img')) {
            $img = $req->file('img')->store('uploads/types', 'public');
            $imgQuery['img']=$img;
            $validate['img']='required | mimes:png,jpg,jpeg,svg|max:100';
        }

        $req->validate($validate);
        try {
            $sqlQuery=[
                'name_en' => $req->input('nameEn'),
                'name_ar' => $req->input('nameAr'),
            ];
            $sqlQuery=array_merge($sqlQuery,$imgQuery);

            $query = Type::where(['id' => $req->input('id')])->update($sqlQuery);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function deleteType(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Type::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    //    Bodies
    function getBodiesDetails(Body $key)
    {

        return view('admin.pages.bodies.edit-body', ['data' => $key, 'page' => 'allBodies']);
    }
    function getBodiesDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Body::where('name_en', 'LIKE', '%' . $request->get('search') . '%');
            } else {
                $data = Body::all();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }
    function postBody(Request $req)
    {

        $req->validate([
            'img' => 'required|mimes:png,jpg,jpeg,svg|max:100',
            'nameEn' => 'required | unique:bodies,name_en',
            'nameAr' => 'required | unique:bodies,name_ar',
        ]);
        try {
            $img = $req->file('img')->store('uploads/bodies', 'public');
            $query = Body::insert([
                'name_en' => $req->input('nameEn'),
                'name_ar' => $req->input('nameAr'),
                'img' => $img,
            ]);
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function editBody(Request $req)
    {
        $imgQuery=[];
        $validate=[
            'nameEn' => 'required |  unique:types,name_en,'.$req->input('id').'',
            'nameAr' => 'required |  unique:types,name_ar,'.$req->input('id').'',
        ];
        if ($req->hasFile('img')) {
            $img = $req->file('img')->store('uploads/bodies', 'public');
            $imgQuery['img']=$img;
            $validate['img']='required | mimes:png,jpg,jpeg,svg|max:100';
        }

        $req->validate($validate);
        try {
            $sqlQuery=[
                'name_en' => $req->input('nameEn'),
                'name_ar' => $req->input('nameAr'),
            ];
            $sqlQuery=array_merge($sqlQuery,$imgQuery);

            $query = Body::where(['id' => $req->input('id')])->update($sqlQuery);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function deleteBody(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Body::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
//    cars
    function getCarDetails(Car $key)
    {

        return view('admin.pages.cars.edit-car', ['data' => $key, 'page' => 'allCars']);
    }
    function getCarDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Car::where('model', 'LIKE', '%' . $request->get('search') . '%')->get()->load(['brands','types']);
            } else {
//                $data = User::latest()->get();
                $data = Car::all()->load(['brands','types']);

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }
    function postCar(Request $req)
    {
        $validate=[
            'meta_image' => 'required|mimes:png,jpg,jpeg,webp|max:200',
            'type' => 'required | integer',
            'brand' => 'required | integer',
            'body' => 'required | integer',
            'model' => 'required',
            'inner' => 'required',
            'outer' => 'required',
            'seats' => 'required | integer',
            'year' => 'required | integer',
            'slug' => 'required | alpha_dash | unique:cars',
            'slugGroup' => 'required ',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
        ];
        for($i=0;$i<$req->input('imgsCount');$i++){
            $validate['imgs'.$i]='required|mimes:png,jpg,jpeg,webp|max:200';
        }
        $req->validate($validate);
        try {

            $img=[];
            for($i=0;$i<$req->input('imgsCount');$i++){

                $img[] = $req->file('imgs'.$i) ->store('uploads/car', 'public');
            }
            $imgs=implode(',',$img);
            $meta_image = $req->file('meta_image')->store('uploads/car', 'public');
            $query = Car::insert([
                'model' => $req->input('model'),
                'type_id' => $req->input('type'),
                'brand_id' => $req->input('brand'),
                'body_id' => $req->input('body'),
                'seats' => $req->input('seats'),
                'daily_price' => $req->input('daily'),
                'hourly_price' => $req->input('hourly'),
                'year' => $req->input('year'),
                'inner_color' => $req->input('inner'),
                'outer_color' => $req->input('outer'),
                'imgs' => $imgs,
                'meta_image' => $meta_image,
                'description_en' => $req->input('descriptionEn'),
                'description_ar' => $req->input('descriptionAr'),
                'slug' => $req->input('slug'),
                'slug_group' => $req->input('slugGroup'),
                'meta_title' => $req->input('meta_title'),
                'meta_keywords' => $req->input('meta_keywords'),
                'meta_description' => $req->input('meta_description'),
            ]);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function editCar(Request $req)
    {
        $imgQuery=[];
        $validate=[
            'type' => 'required | integer',
            'brand' => 'required | integer',
            'body' => 'required | integer',
            'model' => 'required',
            'inner' => 'required',
            'outer' => 'required',
            'seats' => 'required | integer',
            'year' => 'required | integer',
            'slug' => 'required | alpha_dash | unique:cars,slug,'.$req->input('id').'',
            'slugGroup' => 'required ',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
        ];
        if ($req->input('imgsCount')!="0") {
            $img=[];
            for($i=0;$i<$req->input('imgsCount');$i++){

                $img[] = $req->file('imgs'.$i) ->store('uploads/car', 'public');
                $validate['imgs'.$i]='required | mimes:png,jpg,jpeg,webp|max:200';
            }
            $imgs=implode(',',$img);
            $imgQuery['imgs']=$imgs;
        }elseif($req->input('oldImg')!="undefined"){
            $imgQuery['imgs']=$req->input('oldImg');
        }
//        var_dump($imgQuery);
//        exit();
        if ($req->hasFile('meta_image')) {
            $meta_image = $req->file('meta_image')->store('uploads/brands', 'public');
            $imgQuery['meta_image']=$meta_image;
            $validate['meta_image']='required | mimes:png,jpg,jpeg,webp|max:200';
        }
        $req->validate($validate);
        try {
            $sqlQuery=[
                'model' => $req->input('model'),
                'type_id' => $req->input('type'),
                'brand_id' => $req->input('brand'),
                'body_id' => $req->input('body'),
                'seats' => $req->input('seats'),
                'daily_price' => $req->input('daily'),
                'hourly_price' => $req->input('hourly'),
                'year' => $req->input('year'),
                'inner_color' => $req->input('inner'),
                'outer_color' => $req->input('outer'),
                'description_en' => $req->input('descriptionEn'),
                'description_ar' => $req->input('descriptionAr'),
                'slug' => $req->input('slug'),
                'slug_group' => $req->input('slugGroup'),
                'meta_title' => $req->input('meta_title'),
                'meta_keywords' => $req->input('meta_keywords'),
                'meta_description' => $req->input('meta_description'),
            ];
            $sqlQuery=array_merge($sqlQuery,$imgQuery);
//            var_dump($sqlQuery);
//            exit();

            $query = Car::where(['id' => $req->input('id')])->update($sqlQuery);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function deleteCar(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Car::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
//    booking
    function getBookingDetails($key)
    {
        $data = Booking::where([
            'id' => $key,
        ])->get()->load(['cars']);
        return view('admin.pages.bookings.info-booking', ['data' => $data, 'page' => 'allBookings']);
    }
    function getBookingsDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Booking::where('rental_number', 'LIKE', '%' . $request->get('search') . '%')->orWhere('invoice_id', 'LIKE', '%' . $request->get('search') . '%');
            } else {
//                $data = User::latest()->get();
                $data = Booking::all();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }
    function deleteBooking(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Booking::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
}
