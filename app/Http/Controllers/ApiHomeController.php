<?php

namespace App\Http\Controllers;


use App\Models\About;
use App\Models\Blog;
use App\Models\Booking;
use App\Models\Car;
use App\Models\Body;
use App\Models\Brand;
use App\Models\Contact;
use App\Models\Faq;
use App\Models\Social;
use App\Models\Term;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Nette\Utils\DateTime;
use PharIo\Manifest\InvalidEmailException;
use Psy\Util\Json;
use function Symfony\Component\String\u;

class ApiHomeController extends Controller
{
    public $response = [];

    public function getBody()
    {
        $data['car-body'] = Body::get();
        $data['cars'] = Car::get()->load(['brands', 'types', 'bodies']);
        $data['brands'] = Brand::get();
        $data['faq'] = Faq::get();
        return view('index', ['data' => $data]);
    }

    public function search(Request $request)
    {
        $data = Car::where('model', 'LIKE', '%' . $request->search . '%')
            ->get();
        $result = '';
        if (!empty($data[0])) {
            $image = explode(',', $data[0]->imgs);
            foreach ($data as $car) {
                $result .= '<a href="' . route("details", ['group' => $car->slug_group, "car" => $car->slug]) . '">
                        <div class="container-fluid">
                        <div class="row">
                            <div class="col-3 p-0 d-flex">
                                <img src="' . $image[0] . '" class="w-100 m-auto p-1">
                            </div>
                            <div class="col-9 p-1 text-left">
                                <span class="fs-12 text-black">' . $car->model . '</span>
                            </div>
                        </div>
                        </div>
                    </a>';
            }
        } else {
            $result = "<div><span>No Car Found!</span></div>";
        }
        return json_encode($result);
    }

    public function searchPage($key)
    {
        $data['cars'] = Car::where('model', 'LIKE', '%' . $key . '%')
            ->get()->load(['brands', 'bodies']);
        return view('pages.search', ['data' => $data]);;
    }

    public function getCarBySlug($car)
    {
        $data['car'] = Car::where('slug', '=', $car)->first()->load(['brands', 'bodies']);
        return view('pages.car-details', ['data' => $data]);
    }

    public function getBlogBySlug($slug)
    {
        $data['blog'] = Blog::where('slug', '=', $slug)->first();
        return view('pages.blog-details', ['data' => $data]);
    }

    public function getCarsByBrandSlug($brand)
    {
        $data['brand'] = Brand::where('slug', '=', $brand)->first();
        $data['cars'] = Car::where('brand_id', '=', $data['brand']->id)->get()->load(['brands', 'bodies']);
        return view('pages.car-by-brand', ['data' => $data]);
    }

    public function getCarBySlugGroup($group)
    {
        $data['cars'] = Car::where('slug_group', '=', $group)->get()->load(['brands', 'bodies']);
        return view('pages.search', ['data' => $data]);
    }

    public function getBrands()
    {
        $data['brands'] = Brand::get();

        return view('pages.brands', ['data' => $data]);
    }

    public function getBlogs()
    {
        $data['blogs'] = Blog::get();
        return view('pages.blogs', ['data' => $data]);
    }

    public function getCarList()
    {
        $data['car-body'] = Body::get();
        $data['cars'] = Car::get()->load(['brands', 'bodies']);
        $data['brands'] = Brand::get();
        return view('pages.car-list', ['data' => $data]);
    }

    public function getFAQ()
    {
        $data['faq'] = Faq::get();

        return view('pages.faq', ['data' => $data]);
    }

    public function getTerms()
    {
        $terms = Term::get();
        $data['terms'] = $terms;
        return view('pages.terms', ['data' => $data]);
    }

    public function getAbout()
    {
        $data['about'] = About::first();

        return view('pages.about-us', ['data' => $data]);
    }

    public function getContact()
    {
        $data['contact'] = Contact::get();
        $data['social'] = Social::get();

        return view('pages.contact', ['data' => $data]);
    }

    public function booking(Request $req)
    {
        $req->validate([
            'rental_type' => 'required | in:daily,hourly',
            'payment_method' => 'required | in:POD,cash',
            'car_id' => 'required | integer',
            'has_babyseat' => 'required | integer',
            'has_driver' => 'required | integer',
            'from_date' => 'required',
            'to_date' => 'required',
            'customer_name' => 'required ',
            'customer_phone' => 'required ',
            'customer_email' => 'required | email',

        ]);
        try {
            if ($req->has('car_id')) if (!empty($req->input('car_id'))) {
                $car = Car::find($req->input('car_id'));
            } else {

                $this->response['code'] = -10;
                $this->response['data'] = null;
                $this->response['message'] = "Car Not Found";;
                return Json::encode($this->response);
            }
            do {
                $rental_number = rand(100000, 999999);
                $booking_by_rental_number = Booking::where('rental_number', '=', $rental_number)->first();
            } while ($booking_by_rental_number !== null);
            $rentType = $req->input('rental_type');
            $date1 = new DateTime($req->input('from_date'));
            $date2 = new DateTime($req->input('to_date'));
            $startDate = $date1->format('Y-m-d');
            $endDate = $date2->format('Y-m-d');
            $startDate = new DateTime($startDate);
            $endDate = new DateTime($endDate);
//        exit();
            $days = $endDate->diff($startDate)->format('%a');
            $startTime = $date1->format('H:i');
            $endTime = $date2->format('H:i');
            $start = strtotime($startTime);
            $end = strtotime($endTime);
            $hours = ($end - $start) / 3600;
            if ($hours < 0) {
                $days = $days - 1;
                $hours = 24 + $hours;
            }
            if ($hours > 2 || $days > 0) {
                $rentType = "daily";
            }
            if ($rentType == "hourly") {
                $sale_price = $car->hourly_price;
                $rent = $hours * $sale_price;
                if ($req->input('has_babyseat') == '1') {
                    $rent = $rent + 25;
                }
            } else {
                if ($hours > 2) {
                    $days = $days + 1;
                    $hours = 0;
                } elseif ($days == 0) {
                    $days = 1;
                    $hours = 0;
                }
                $rent = $days * $car->daily_price;
                if ($req->input('has_babyseat') == '1') {
                    $rent = $rent + (25 * $days);
                }
            }
            $invoiceId=0;
            $state=1;
            $rent = $rent + ($rent * 5 / 100);

            if ($req->input('payment_method') == 'cash') {
                $invoiceItems[] = [
                    'ItemName' => 'Rent ' . $car->model . '',
                    'Quantity'  => '1', //Item's quantity
                    'UnitPrice' => $rent, //Price per item
                ];
                $paymentData[] = [
                    "CustomerName" => $req->input('customer_name'),
                    "NotificationOption" => "lnk",

                    "CustomerMobile" => $req->input('customer_phone'),
                    "CustomerEmail" => $req->input('customer_email'),
                    "InvoiceValue" => $rent,
                    "DisplayCurrencyIso" => "aed",
                    "CallBackUrl" => "https://sample-success.com",
                    "ErrorUrl" => "https://sample-failure.com",
                    "Language" => "En",
                    'InvoiceItems' =>$invoiceItems
                ];
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://apitest.myfatoorah.com/v2/SendPayment',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($paymentData[0]),
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: Bearer rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL',
                        'Content-Type: application/json',
                        'Cookie: ApplicationGatewayAffinity=61939aeb6b7c5f38617144d210b01e24; ApplicationGatewayAffinityCORS=61939aeb6b7c5f38617144d210b01e24'
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
//                echo $response;
//                var_dump(json_encode($invoiceItems[0]));
//
                $response=json_decode($response,true);
                $invoiceId= $response['Data']['InvoiceId'];
                $state=0;

            }


            $query = Booking::insert([
                'rental_type' => $rentType,
                'invoice_id' => $invoiceId,
                'rental_number' => $rental_number,
                'payment_method' => $req->input('payment_method'),
                'car_id' => $req->input('car_id'),
                'has_babyseat' => $req->input('has_babyseat'),
                'has_driver' => $req->input('has_driver'),
                'from_date' => $req->input('from_date'),
                'to_date' => $req->input('to_date'),
                'customer_name' => $req->input('customer_name'),
                'customer_phone' => $req->input('customer_phone'),
                'customer_email' => $req->input('customer_email'),
                'total_price' => $rent,
                'state' => $state,
            ]);

            $this->response['code'] = 1;
            $this->response['data'] = $rental_number;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    public function CarsFilter(Request $request)
    {
        $rent_type = ($request->rent_type == 0) ? 'hourly' : 'daily';
        $min_price = $request->min_price;
        $max_price = $request->max_price;
        $brands = $request->brands;
        $any_brand = true;
        if ($brands != '0' && $brands != '[]') {
            $brands = json_decode($request->brands);
            $any_brand = false;
        }
        if ($rent_type == 'hourly') {

            if ($any_brand) {
                $cars = Car::where('hourly_price', '>=', $min_price)
                    ->where('hourly_price', '<=', $max_price)
                    ->get();

            } else {
                $cars = Car::where('hourly_price', '>=', $min_price)
                    ->where('hourly_price', '<=', $max_price)
                    ->whereIn('brand_id', $brands)
                    ->get();

            }
        } else {
            if ($any_brand) {
                $cars = Car::where('daily_price', '>=', $min_price)
                    ->where('daily_price', '<=', $max_price)
                    ->get();

            } else {
                $cars = Car::where('daily_price', '>=', $min_price)
                    ->where('daily_price', '<=', $max_price)
                    ->whereIn('brand_id', $brands)
                    ->get();

            }
        }
        if (sizeof($cars) == 0) {
            return json_encode('<div class="col-12"><div class="alert alert-dark mt-3 mx-5" role="alert">No Cars Found!</div></div>');
        } else {
            $response = '';

            foreach ($cars as $car) {
                $imgs = explode(',', $car->imgs);
                $response .= '<div class="col-12 col-md-4 my-2 ' . $car->bodies->name_en . ' ALL car-selector">
    <img class="card-brand" src="' . url("/") . '/' . $car->brands->img . '">
    <div class="car-card text-center">
     <a   href="' . route('details', ['group' => $car->slug_group, "car" => $car->slug]) . '">
        <div class="car-slider">';
                foreach ($imgs as $img) {
                    $response .= '<div>
                    <img src="' . url("/") . '/' . $img . '" class="w-100">
                </div>';
                }
                $response .= '</div></a>
 <a   href="' . route('details', ['group' => $car->slug_group, "car" => $car->slug]) . '">
        <h4 class="gold pt-1">' . $car->model . '</h4>
        </a>
        <div class="car-card-details text-center">

            <div class="details-2">
                <div class="row px-3 pt-1">
                    <div class="col-12 text-white  d-flex justify-content-center mb-3">
                       <span class="">' . $car->hourly_price . ' AED <em class="gray3" style="font-weight:100; ">Hourly</em> </span><div class="mx-3" style="width: 2px; background-color: var(--gold)"></div>   <span class="">' . $car->daily_price . ' AED <em style="font-weight:100; ">Day</em></span>
                    </div>
                </div>
                <div class="container-fluid px-0 text-white">
                    <div class="row" style="direction: ltr">
                        <div class="col-3 pr-1">
                            <a class="btn btn-outline-dark d-inline-block  w-100 py-2 btn-first"
                               href="https://api.whatsapp.com/send?phone=" target="_blank"><i
                                        class="fa fa-whatsapp"></i> Whatsapp
                            </a>
                        </div>
                        <div class="col-3 px-1">
                            <a class="btn btn-outline-dark d-inline-block  w-100 py-2 btn-2"
                               href="tel:+"><i
                                        class="fa fa-phone"></i> Call
                            </a>
                        </div>
                        <div class="col-3 px-1">
                            <a class="btn btn-outline-dark d-inline-block  w-100 py-2 btn-3"
                               href="' . route('details', ['group' => $car->slug_group, "car" => $car->slug]) . '"><i
                                        class="fa fa-info-circle"></i> Details
                            </a>
                        </div>
                        <div class="col-3 pl-1">
                            <a class="btn btn-outline-dark d-inline-block  w-100 py-2 btn-last bookNow"
                               data-carid="' . $car->id . '"
                               data-name="' . $car->brands->name . ' ' . $car->model . '"
                               data-daily="' . $car->hourly_price . '"
                               data-hourly="' . $car->daily_price . '"
                               data-fee=""><i
                                        class="fa fa-car"></i> Book
                            </a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>';
            }
            return json_encode($response);
        }
    }

    function sendMail(Request $req)
    {
        $req->validate([
            'email' => 'email',
            'name' => 'required',
            'phone' => 'required',
        ]);
        try {
            Mail::send([], [], function ($message) use ($req) {
                if (($req->input('car')) && !empty($req->input('car'))) {
                    $message->from(['adnankiswani123@gmail.com'])->to(['adnankiswani123@gmail.com' => 'adnan'])->subject('Luxury')->setBody('<h1>Luxury Car</h1><br><h2>Name:' . $req->input("name") . '</h2><br><h2>Email:' . $req->input("email") . '</h2><br><h2>phone:' . $req->input("phone") . '</h2><br><h2>Message:' . $req->input("message") . '</h2><br><h2>Car:' . $req->input("car") . '</h2><br><h2>Link:<a href="' . $req->input("link") . '">' . $req->input("link") . '</a></h2>', 'text/html');
                } elseif (($req->input('message')) && !empty($req->input('message'))) {
                    $message->from(['adnankiswani123@gmail.com' => 'adnan'])->to(['adnankiswani123@gmail.com' => 'adnan'])->subject('Luxury')->setBody('<h1>Luxury Car</h1><br><h2>Name:' . $req->input("name") . '</h2><br><h2>Email:' . $req->input("email") . '</h2><br><h2>phone:' . $req->input("phone") . '</h2><br><h2>Message:' . $req->input("message") . '</h2>', 'text/html');
//var_dump('aaaa');
                } else {
                    $message->from(['adnankiswani123@gmail.com' => 'adnan'])->to(['adnankiswani123@gmail.com' => 'adnan'])->subject('Luxury')->setBody('<h1>Luxury Car</h1><br><h2>Name:' . $req->input("name") . '</h2><br><h2>Email:' . $req->input("email") . '</h2><br><h2>phone:' . $req->input("phone") . '</h2>', 'text/html');
                }
            });
//           var_dump($mail);
//           exit();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (InvalidEmailException $ex) {
            $this->response['code'] = -100;
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }
        return Json::encode($this->response);
    }

}
