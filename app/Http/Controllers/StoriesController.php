<?php

namespace App\Http\Controllers;

use App\Models\Story;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Psy\Util\Json;
use Yajra\DataTables\DataTables;

class StoriesController extends Controller
{
    //
    function getStoriesDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Story::where('title', 'LIKE', '%' . $request->get('search') . '%')->get();
            } else {
                $data = Story::all();
            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }

    
    function deleteStory(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Story::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    
    function addStory(Request $req)
    {
        $validate=[
            'image' => 'required|mimes:png,jpg,jpeg,webp|max:200',
            'title' => 'required | string',
            'description' => 'required | string',
            'slug' => 'required | alpha_dash | unique:stories',
        ];
        $req->validate($validate);
        try {

            $image = $req->file('image')->store('uploads/story', 'public');
            $query = Story::insert([
                'title' => $req->input('title'),
                'slug' => $req->input('slug'),
                'image' => $image,
                'description' => $req->input('description'),
                'slug' => $req->input('slug'),
            ]);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    
    function getStoryDetails(Story $key)
    {
        return view('admin.pages.story.edit-story', ['data' => $key, 'page' => 'all-stories']);
    }

    
    function editStory(Request $req)
    {
        $imgQuery=[];
        $validate=[
            'image' => 'required|mimes:png,jpg,jpeg,webp|max:200',
            'title' => 'required | string',
            'description' => 'required | string',
            'slug' => 'required | alpha_dash | unique:stories,slug,'.$req->input('id').'',
        ];
        if ($req->hasFile('image')) {
            $image = $req->file('image')->store('uploads/story', 'public');
            $imgQuery['image']=$image;
            $validate['image']='required | mimes:png,jpg,jpeg,webp|max:200';
        }
        $req->validate($validate);
        try {
            $sqlQuery=[
                'title' => $req->input('title'),
                'slug' => $req->input('slug'),
                'description' => $req->input('description'),
            ];
            $sqlQuery=array_merge($sqlQuery,$imgQuery);
            $query = Story::where(['id' => $req->input('id')])->update($sqlQuery);
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }
        return Json::encode($this->response);
    }
}
