<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Blog;
use App\Models\Contact;
use App\Models\Faq;
use App\Models\Social;
use App\Models\Term;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Psy\Util\Json;
use Yajra\DataTables\DataTables;

class PagesController extends Controller
{
    public $response = [];

    //    blogs
    function getBlogsDetails(Blog $key)
    {

        return view('admin.pages.blogs.edit-blog', ['data' => $key, 'page' => 'allBlogs']);
    }

    function getBlogsDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Blog::where('title_en', 'LIKE', '%' . $request->get('search') . '%');
            } else {
                $data = Blog::all();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }

    function postBlog(Request $req)
    {

        $req->validate([
            'cover' => 'required|mimes:png,jpg,jpeg,webp|max:200',
            'meta_image' => 'required|mimes:png,jpg,jpeg,webp|max:200',
            'titleEn' => 'required',
            'titleAr' => 'required',
            'descriptionEn' => 'required',
            'descriptionAr' => 'required',
            'slug' => 'required | unique:blogs',
            'meta_title' => 'required ',
            'meta_keywords' => 'required ',
        ]);
        try {
            $cover = $req->file('cover')->store('uploads/blogs', 'public');
            $meta_image = $req->file('meta_image')->store('uploads/blogs', 'public');
            $query = Blog::insert([
                'slug' => $req->input('slug'),
                'title_en' => $req->input('titleEn'),
                'title_ar' => $req->input('titleAr'),
                'description_en' => $req->input('descriptionEn'),
                'description_ar' => $req->input('descriptionAr'),
                'cover' => $cover,
                'meta_image' => $meta_image,
                'meta_title' => $req->input('meta_title'),
                'meta_keywords' => $req->input('meta_keywords'),
                'meta_description' => $req->input('meta_description'),
            ]);
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function editBlog(Request $req)
    {
        $imgQuery = [];
        $validate = [
            'titleEn' => 'required',
            'titleAr' => 'required',
            'descriptionEn' => 'required',
            'descriptionAr' => 'required',
            'meta_title' => 'required ',
            'meta_keywords' => 'required ',
            'slug' => 'required |  unique:blogs,slug,' . $req->input('id') . '',
        ];
        if ($req->hasFile('cover')) {
            $cover = $req->file('cover')->store('uploads/blogs', 'public');
            $imgQuery['cover'] = $cover;
            $validate['cover'] = 'required | mimes:png,jpg,jpeg,webp | max:200';
        }
        if ($req->hasFile('meta_image')) {
            $meta_image = $req->file('meta_image')->store('uploads/blogs', 'public');
            $imgQuery['meta_image']=$meta_image;
            $validate['meta_image']='required | mimes:png,jpg,jpeg,webp|max:200';
        }
        $req->validate($validate);
        try {
            $sqlQuery = [
                'title_en' => $req->input('titleEn'),
                'title_ar' => $req->input('titleAr'),
                'description_en' => $req->input('descriptionEn'),
                'description_ar' => $req->input('descriptionAr'),
                'meta_title' => $req->input('meta_title'),
                'meta_keywords' => $req->input('meta_keywords'),
                'meta_description' => $req->input('meta_description'),
            ];
            $sqlQuery = array_merge($sqlQuery, $imgQuery);

            $query = Blog::where(['id' => $req->input('id')])->update($sqlQuery);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function deleteBlog(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Blog::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    //    faqs
    function getFaqsDetails(Faq $key)
    {

        return view('admin.pages.faqs.edit-faq', ['data' => $key, 'page' => 'allFaqs']);
    }

    function getFaqsDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Faq::where('question_en', 'LIKE', '%' . $request->get('search') . '%');
            } else {
                $data = Faq::all();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }

    function postFaq(Request $req)
    {

        $req->validate([
            'q1En' => 'required',
            'q1Ar' => 'required',
            'a1En' => 'required',
            'a1Ar' => 'required',
        ]);
        try {

            for ($i = 1; $i <= $req->input('i'); $i++) {
                if (!empty($req->input('q' . $i . 'En')) && !empty($req->input('q' . $i . 'Ar')) && !empty($req->input('a' . $i . 'En')) && !empty($req->input('a' . $i . 'Ar'))) {
                    $query = Faq::insert([
                        'question_en' => $req->input('q' . $i . 'En'),
                        'question_ar' => $req->input('q' . $i . 'Ar'),
                        'answer_en' => $req->input('a' . $i . 'En'),
                        'answer_ar' => $req->input('a' . $i . 'Ar'),


                    ]);
                }
            }
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function editFaq(Request $req)
    {

        $validate = [
            'qEn' => 'required',
            'qAr' => 'required',
            'aEn' => 'required',
            'aAr' => 'required',

        ];

        $req->validate($validate);
        try {
            $sqlQuery = [
                'question_en' => $req->input('qEn'),
                'question_ar' => $req->input('qAr'),
                'answer_en' => $req->input('aEn'),
                'answer_ar' => $req->input('aAr'),
            ];


            $query = Faq::where(['id' => $req->input('id')])->update($sqlQuery);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function deleteFaq(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Faq::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    //    socials
    function getSocialsDetails(Social $key)
    {

        return view('admin.pages.socials.edit-social', ['data' => $key, 'page' => 'allSocials']);
    }

    function getSocialsDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Social::where('icon', 'LIKE', '%' . $request->get('search') . '%');
            } else {
                $data = Social::all();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }

    function postSocial(Request $req)
    {

        $req->validate([
            'icon1' => 'required',
            'url1' => 'required',

        ]);
        try {

            for ($i = 1; $i <= $req->input('i'); $i++) {
                if (!empty($req->input('icon' . $i)) && !empty($req->input('url' . $i))) {
                    $query = Social::insert([
                        'icon' => $req->input('icon' . $i),
                        'url' => $req->input('url' . $i),
                    ]);
                }
            }
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function editSocial(Request $req)
    {

        $validate = [
            'icon' => 'required',
            'url' => 'required',
        ];

        $req->validate($validate);
        try {
            $sqlQuery = [
                'icon' => $req->input('icon'),
                'url' => $req->input('url'),
            ];


            $query = Social::where(['id' => $req->input('id')])->update($sqlQuery);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function deleteSocial(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Social::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }


    //    contacts
    function getContactsDetails(Contact $key)
    {

        return view('admin.pages.contacts.edit-contact', ['data' => $key, 'page' => 'allContacts']);
    }

    function getContactsDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Contact::where('title', 'LIKE', '%' . $request->get('search') . '%');
            } else {
                $data = Contact::all();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }

    function postContact(Request $req)
    {

        $req->validate([
            'icon1' => 'required',
            'url1' => 'required',
            'type1' => 'required',
            'title1' => 'required',

        ]);
        try {

            for ($i = 1; $i <= $req->input('i'); $i++) {
                if (!empty($req->input('icon' . $i)) && !empty($req->input('url' . $i))&& !empty($req->input('type' . $i)) && !empty($req->input('title' . $i))) {
                    $query = Contact::insert([
                        'icon' => $req->input('icon' . $i),
                        'url' => $req->input('url' . $i),
                        'type' => $req->input('type' . $i),
                        'title' => $req->input('title' . $i),
                    ]);
                }
            }
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function editContact(Request $req)
    {

        $validate = [
            'icon' => 'required',
            'url' => 'required',
            'type' => 'required',
            'title' => 'required',
        ];

        $req->validate($validate);
        try {
            $sqlQuery = [
                'icon' => $req->input('icon'),
                'url' => $req->input('url'),
                'type' => $req->input('type'),
                'title' => $req->input('title'),
            ];


            $query = Contact::where(['id' => $req->input('id')])->update($sqlQuery);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function deleteContact(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Contact::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }


    //    terms
    function getTermsDetails(Term $key)
    {

        return view('admin.pages.terms.edit-term', ['data' => $key, 'page' => 'allTerms']);
    }

    function getTermsDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search'))) {
                $data = Term::where('title_en', 'LIKE', '%' . $request->get('search') . '%');
            } else {
                $data = Term::all();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }

    function postTerm(Request $req)
    {

        $req->validate([
            'titleEn1' => 'required',
            'titleAr1' => 'required',
            'descriptionEn1' => 'required',
            'descriptionAr1' => 'required',

        ]);
        try {

            for ($i = 1; $i <= $req->input('i'); $i++) {
                if (!empty($req->input('titleEn' . $i)) && !empty($req->input('titleAr' . $i)) && !empty($req->input('descriptionEn' . $i)) && !empty($req->input('descriptionAr' . $i))) {
                    $query = Term::insert([
                        'title_en' => $req->input('titleEn' . $i),
                        'title_ar' => $req->input('titleAr' . $i),
                        'description_en' => $req->input('descriptionEn' . $i),
                        'description_ar' => $req->input('descriptionAr' . $i),
                    ]);
                }
            }
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function editTerm(Request $req)
    {

        $validate = [
            'titleEn' => 'required',
            'titleAr' => 'required',
            'descriptionEn' => 'required',
            'descriptionAr' => 'required',
        ];

        $req->validate($validate);
        try {
            $sqlQuery = [
                'title_en' => $req->input('titleEn'),
                'title_ar' => $req->input('titleAr'),
                'description_en' => $req->input('descriptionEn'),
                'description_ar' => $req->input('descriptionAr'),
            ];


            $query = Term::where(['id' => $req->input('id')])->update($sqlQuery);

            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function deleteTerm(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = Term::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }


//    about
    function getAboutDetails()
    {
        $data = About::all();
        if (isset($data[0])) {
            $data = $data[0]->toArray();
        }
        return view('admin.pages.about.about', ['data' => $data, 'page' => 'about']);
    }

    function editAbout(Request $req)
    {
        $imgQuery = [];
        $validate = [
            'titleEn' => 'required',
            'titleAr' => 'required',
            'descriptionEn' => 'required',
            'descriptionAr' => 'required',
        ];
        if ($req->hasFile('cover')) {
            $cover = $req->file('cover')->store('uploads/about', 'public');
            $imgQuery['cover'] = $cover;
            $validate['cover'] = 'required | mimes:png,jpg,jpeg,webp | max:200';
        }
        $req->validate($validate);
        try {
            $sqlQuery = [
                'title_en' => $req->input('titleEn'),
                'title_ar' => $req->input('titleAr'),
                'description_en' => $req->input('descriptionEn'),
                'description_ar' => $req->input('descriptionAr'),
            ];

            $sqlQuery = array_merge($sqlQuery, $imgQuery);
            if($req->input('id')!='undefined'){
                $query = About::where(['id' => $req->input('id')])->update($sqlQuery);
            }else{
                $query=About::insert($sqlQuery);
            }


            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
}
