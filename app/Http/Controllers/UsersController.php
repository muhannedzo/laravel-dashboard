<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Psy\Util\Json;
use \Yajra\DataTables\DataTables;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersController extends Controller
{
    public $response = [];


    function getUserDetails(User $key)
    {


        $roles = Role::all();
        $userRole = $key->getRoleNames()->toArray();
        return view('admin.pages.users.edit-user', ['data' => $key, 'page' => 'edit-user', 'roles' => $roles, 'userRole' => $userRole[0]]);
    }

    function getUsersDatatable(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->get('search')) && !empty($request->get('stateFilter'))) {
                $data = User::join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                    ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')->select('users.*', 'roles.name as role_name', 'users.name as user_name')->where('state', $request->get('stateFilter'))->Where('users.name', 'LIKE', '%' . $request->get('search') . '%');
            } elseif (!empty($request->get('search'))) {
                $data = User::join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                    ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')->select('users.*', 'roles.name as role_name', 'users.name as user_name')->where('users.name', 'LIKE', '%' . $request->get('search') . '%')->orWhere('users.email', 'LIKE', '%' . $request->get('search') . '%');
            } elseif (!empty($request->get('stateFilter'))) {
                $data = User::join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                    ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')->select('users.*', 'roles.name as role_name', 'users.name as user_name')->where('users.state', $request->get('stateFilter'));
            } else {
//                $data = User::latest()->get();
                $data = User::join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                    ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')->select('users.*', 'roles.name as role_name', 'users.name as user_name')->get();

            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }

    function postUser(Request $req)
    {

        $req->validate([
            'avatar' => 'required|mimes:png,jpg,jpeg|max:2048',
            'name' => 'required',
            'role' => 'required',
            'email' => 'required |email|unique:users',
            'password' => 'required | min:6',
        ]);
        try {
            $avatar = $req->file('avatar')->store('uploads', 'public');
            $query = User::insert([
                'name' => $req->input('name'),
                'avatar' => $avatar,
                'email' => $req->input('email'),
                'password' => Hash::make($req->input('password')),
            ]);
            $user = User::where([
                'email' => $req->input('email'),
            ])->first();
            $user->assignRole($req->input('role'));
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    public static function getRoles()
    {


        return $roles = Role::all();


    }

    function patchUser(Request $req)
    {
        $req->validate([
            'id' => 'required',
            'name' => 'required',
            'role' => 'required',
            'email' => 'required |email',
        ]);
        $emailCheck = User::where(['email' => $req->input('email')])->where('id', '!=', $req->input('id'))->get();
        if (!empty($emailCheck[0])) {
            $response = response(ResponseController::response(-20, 'Email Already Exist', null), 301);

        } else {
            try {
                if ($req->hasFile('avatar')) {
                    $avatar = $req->file('avatar')->store('uploads', 'public');

                    $query = User::where(['id' => $req->input('id')])->update([
                        'name' => $req->input('name'),
                        'avatar' => $avatar,
                        'email' => $req->input('email')]);
                } else {
                    $query = User::where(['id' => $req->input('id')])->update([
                        'name' => $req->input('name'),
                        'email' => $req->input('email')]);
                }
                $user = User::where([
                    'id' => $req->input('id'),
                ])->first();
                $user->syncRoles($req->input('role'));
                $this->response['code'] = 1;
                $this->response['data'] = null;
                $this->response['message'] = "Success";
            } catch (QueryException $ex) {
//            var_dump($ex);
//            exit();
                $this->response['code'] = $ex->getCode();
                $this->response['data'] = null;
                $this->response['message'] = $ex->getMessage();
            }
        }


        return Json::encode($this->response);
    }

    function deleteUser(Request $req)
    {

        $req->validate([
            'id' => 'required',
        ]);
        try {
            $query = User::where([
                'id' => $req->input('id'),
            ])->delete();
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
//            var_dump($ex);
//            exit();
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function patchUserState(Request $req)
    {
        $req->validate([
            'id' => 'required',
            'state' => 'required',
        ]);
        try {
            $query = User::where([
                'id' => $req->input('id'),
            ])->update(['state' => $req->input('state')]);
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
//            var_dump($ex);
//            exit();
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function logIn(Request $req)
    {
        $req->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        try {
            $user = User::where(['users.email' => $req->input('email')])->first();
            if (isset($user) && (Hash::check($req->input('password'), $user['password']))) {
                if ($user['state'] == 1) {
                    Auth::login($user);
//                    session(['user' => $user]);
                    $this->response['code'] = 1;
                    $this->response['data'] = null;
                    $this->response['message'] = "Success";
                } else {
                    $this->response['code'] = -20;
                    $this->response['data'] = null;
                    $this->response['message'] = "User Unactivated";
                }

            } else {
                $this->response['code'] = -2;
                $this->response['data'] = null;
                $this->response['message'] = "User name or password incorrect";
            }
        } catch (QueryException $ex) {
//            var_dump($ex);
//            exit();
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }

    function logOut(Request $req)
    {
//        var_dump($req->session()->get('user'));
//        exit();
        Auth::logout();
//        $req->session()->forget('user');
        $this->response['code'] = 1;
        $this->response['data'] = null;
        $this->response['message'] = "Success";

        return Json::encode($this->response);
    }


    function changeLang(Request $req)
    {
        Session::put('locale', $req->input('lang'));
        $this->response['code'] = 1;
        $this->response['data'] = null;
        $this->response['message'] = "Success";
        return Json::encode($this->response);
    }

}
