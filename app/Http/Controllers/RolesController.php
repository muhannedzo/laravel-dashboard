<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Psy\Util\Json;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class RolesController extends Controller
{
    public $response=[];
    function getRolesDatatable(Request $request)
    {
        if ($request->ajax()) {
           if (!empty($request->get('search'))) {
                $data = Role::where('name', 'LIKE', '%' . $request->get('search') . '%');
            }  else {
                $data = Role::all();
            }
            return DataTables::of($data)->make(true);
        }

        return view('index');
    }
    function createRoleView(Request $request){
        $permissions=Permission::all();
        return view('admin.pages.roles.create-role',['page'=>'create-role','permissions'=>$permissions]);
    }
    function editRoleView($id){
        $permissions=Permission::all();
        $role=Role::findById($id);
        $name=$role->name;
        $userPermissions=$role->permissions;
        $permissionsName=[];
        foreach ($userPermissions as $permission){
            $permissionsName[]=$permission->name;
        }
        return view('admin.pages.roles.edit-role',['permissions'=>$permissionsName,'allPermission'=>$permissions,'name'=>$name,'id'=>$id]);
    }
    function postRole(Request $request){
        $request->validate([
            'name' => 'required | unique:roles',
        ]);

        try {
            $role = Role::create(['name' => $request->input('name')]);
            if(!empty($request->input('permissions'))){
                $permissions= explode(',',$request->input('permissions'));
              foreach ($permissions as $permission){
                  $role->givePermissionTo($permission);
              }
            }



            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function deleteRole(Request $request){
        $request->validate([
            'id' => 'required',
        ]);

        try {
            $role = Role::where(['id' => $request->input('id')])->delete();
            if(!empty($request->input('permissions'))){
                $permissions= explode(',',$request->input('permissions'));
              foreach ($permissions as $permission){
                  $role->givePermissionTo($permission);
              }
            }



            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
//            var_dump($ex);
//            exit();
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
    function patchRole(Request $request){


        try {
            $role = Role::findById($request->input('id'));

            if(!empty($request->input('permissions'))){
                $permissions= explode(',',$request->input('permissions'));
                $role->syncPermissions($permissions);

            }
            // else{
            //     $role->syncPermissions([]);
            // }
            $this->response['code'] = 1;
            $this->response['data'] = null;
            $this->response['message'] = "Success";
        } catch (QueryException $ex) {
//            var_dump($ex);
//            exit();
            $this->response['code'] = $ex->getCode();
            $this->response['data'] = null;
            $this->response['message'] = $ex->getMessage();
        }


        return Json::encode($this->response);
    }
}
