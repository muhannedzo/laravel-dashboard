<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;


class APIUsersController extends Controller
{
    public $response = [];

    //
    function getUsers()
    {

        $users = User::all();
        return $response = ResponseController::response(1, 'Success', $users);
    }

    function getUserById(User $key)
    {
        $user = $key;


        return $response = ResponseController::response(1, 'Success', $user);
    }

    function addUser(Request $req)
    {
        $rules = ([
            'avatar' => 'required|mimes:png,jpg,jpeg|max:2048',
            'name' => 'required',
            'role' => 'required',
            'email' => 'required |email|unique:users',
            'password' => 'required | min:6',
        ]);
        $validator = Validator::make($req->all(), $rules);
        if ($validator->fails()) {
            $response = response(ResponseController::response(-10, 'Validation Error', $validator->errors()), 400);

        } else {
            try {
                $avatar = $req->file('avatar')->store('uploads', 'public');
                $query = User::insert([
                    'name' => $req->input('name'),
                    'avatar' => $avatar,
                    'email' => $req->input('email'),
                    'password' => Hash::make($req->input('password')),
                ]);
                $user = User::where([
                    'email' => $req->input('email'),
                ])->first();
                $user->assignRole($req->input('role'));
                $response = ResponseController::response(1, 'Success', null);

            } catch (QueryException $ex) {
                $response = response(ResponseController::response($ex->getCode(), $ex->getMessage(), null), 402);

            }
        }

        return $response;
    }

    function editUser($id, Request $req)
    {

        $rules = ([
            'name' => 'required',
            'role' => 'required',
            'email' => 'required |email',
        ]);
        $emailCheck = User::where(['email' => $req->input('email')])->where('id', '!=', $id)->get();
//        var_dump($emailCheck);
//        exit();
        if (!empty($emailCheck[0])) {
            $response = response(ResponseController::response(-20, 'Email Already Exist', null), 301);

        } else {
            $validator = Validator::make($req->all(), $rules);
            if ($validator->fails()) {
                $response = response(ResponseController::response(-10, 'Validation Error', $validator->errors()), 400);

            } else {
                try {
                    if ($req->hasFile('avatar')) {
                        $avatar = $req->file('avatar')->store('uploads', 'public');
                        $query = User::where(['id' => $id])->update([
                            'name' => $req->input('name'),
                            'avatar' => $avatar,
                            'email' => $req->input('email'),
                        ]);

                    } else {
                        $query = User::where(['id' => $id])->update([
                            'name' => $req->input('name'),
                            'email' => $req->input('email'),
                        ]);
                    }


                    $user = User::where([
                        'email' => $req->input('email'),
                    ])->first();
                    $user->syncRoles($req->input('role'));
                    $response = ResponseController::response(1, 'Success', null);

                } catch (QueryException $ex) {
                    $response = response(ResponseController::response($ex->getCode(), $ex->getMessage(), null), 402);

                }
            }
        }

        return $response;
    }

    function deleteUser($id)
    {
        try {
            User::where([
                'id' => $id,
            ])->delete();
            $response = ResponseController::response(1, 'Success', null);
        } catch (QueryException $ex) {
            $response = response(ResponseController::response($ex->getCode(), $ex->getMessage(), null), 402);
        }
        return $response;
    }

    function userStateChange(Request $req)
    {
        $rules = ([
            'id' => 'required',
            'state' => 'required | int',
        ]);
        $validator = Validator::make($req->all(), $rules);
        if ($validator->fails()) {
            $response = response(ResponseController::response(-10, 'Validation Error', $validator->errors()), 400);

        } else {
            try {
                $user = User::where([
                    'id' => $req->input('id'),
                ])->update(['state' => $req->input('state')]);

                    $response = ResponseController::response(1, 'Success', null);

            } catch (QueryException $ex) {
                $response = response(ResponseController::response($ex->getCode(), $ex->getMessage(), null), 402);
            }
        }

        return $response;
    }
    function logIn(Request $req)
    {
        $rules = ([
            'email' => 'required |email',
            'password' => 'required | min:6',
        ]);
        $validator = Validator::make($req->all(), $rules);
        if ($validator->fails()) {
            $response = response(ResponseController::response(-10, 'Validation Error', $validator->errors()), 400);

        } else {
            try {
                $user = User::where([
                    'email' => $req->input('email'),
                ])->first();
                if (isset($user) && (Hash::check($req->input('password'), $user['password']))) {
                    if($user['state']=='1') {
                        $userDate['token'] = $user->createToken($req->email)->plainTextToken;
                        $userDate['data'] = $user;
                        $response = ResponseController::response(1, 'Success', $userDate);
                    }else{
                        $response = response(ResponseController::response(-20, 'User unactivated', null), 420);

                    }

                } else {
                    $response = response(ResponseController::response(-3, 'User name or password incorrect', $validator->errors()), 400);

                }
            } catch (QueryException $ex) {
                $response = response(ResponseController::response($ex->getCode(), $ex->getMessage(), null), 402);
            }
        }

        return $response;
    }

    function logout(Request $req)
    {
        $rules = ([
            'id' => 'required',
        ]);
        $validator = Validator::make($req->all(), $rules);
        if ($validator->fails()) {
            $response = response(ResponseController::response(-10, 'Validation Error', $validator->errors()), 400);

        } else {
            try {
                $req->user()->currentAccessToken()->delete();

                $response = ResponseController::response(1, 'Success', $req);

            } catch (QueryException $ex) {
                $response = response(ResponseController::response($ex->getCode(), $ex->getMessage(), null), 402);
            }
        }

        return $response;
    }
}
