<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    public function cars(){
        return $this->hasOne(Car::class, 'id', 'car_id');
    }
}
