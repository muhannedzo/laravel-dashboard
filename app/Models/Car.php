<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    use Sluggable;
    protected $guarded = [];
    public function brands(){
        return $this->hasOne(Brand::class, 'id', 'brand_id');
    } public function types(){
        return $this->hasOne(Type::class, 'id', 'type_id');
    } public function bodies(){
        return $this->hasOne(Body::class, 'id', 'body_id');
    }
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
