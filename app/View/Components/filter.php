<?php

namespace App\View\Components;

use Illuminate\View\Component;

class filter extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $brands;
    public $brandsIdArray;
    public function __construct($brands,$brandsIdArray)
    {
        $this->brands=$brands;
        $this->brandsIdArray=$brandsIdArray;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.filter');
    }
}
