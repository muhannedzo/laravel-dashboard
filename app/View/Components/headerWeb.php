<?php

namespace App\View\Components;

use Illuminate\View\Component;

class headerWeb extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $filter;
    public $description;
    public $title;
    public $keywords;
    public $image;
    public function __construct($filter,$description,$title,$keywords,$image)
    {
        $this->filter=$filter;
        $this->description=$description;
        $this->title=$title;
        $this->keywords=$keywords;
        $this->image=$image;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.header-web');
    }
}
