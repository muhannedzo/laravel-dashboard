$(document).ready(function () {
    new WOW().init();
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    $(document).on('click', '.logout', function () {
        $.ajax( base_url+'logout', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (data, status, xhr) {
                if (data['code'] == '1') {

                    window.location.replace(base_url+'admin');
                } else {
                    Toast.fire({
                        icon: 'warning',
                        title: data["message"]
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                responseError(jqXhr, textStatus, errorMessage)
            }
        });
    });
    $(document).on('click', '.lang', function () {
        var lang=$(this).data('lang');
        $.ajax(''+base_url+'changeLang', {
            type: 'POST',  // http method
            dataType: "json",
            data:{
                lang:lang
            },
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (data, status, xhr) {
                if (data['code'] == '1') {

                    window.location.reload();
                } else {
                    Toast.fire({
                        icon: 'warning',
                        title: data["message"]
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                responseError(jqXhr, textStatus, errorMessage)
            }
        });
    });
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

var metaDesc=1;
    $('#metaDescription').trumbowyg().on('tbwchange',function () {
        var formData = new FormData();
        var text = $('#metaDescription').val();
        formData.append('text', text);
        $.ajax(base_url + 'admin/text', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data, status, xhr) {
                // $('.SEODetails').fadeIn();
                if(metaDesc===1) {
                    $('#metaDescription').after('<div class="text-white my-2 SEODetails SEODescription">\n' +
                        '                            <span>Readability: <span class="readability"></span></span>\n' +
                        '                            <span>Text Length: <span class="textLength"></span></span>\n' +
                        '                            <span>Letters Count: <span class="lettersCount"></span></span>\n' +
                        '                            <span>Words Count: <span class="wordsCount"></span></span>\n' +
                        '                            <span>Sentences Count: <span class="sentencesCount"></span></span>\n' +
                        '                        </div>');
                }
                metaDesc=2;
                $('.SEODescription').find('.readability').html(data.data.readability);
                $('.SEODescription').find('.textLength').html(data.data.textLength);
                $('.SEODescription').find('.lettersCount').html(data.data.lettersCount);
                $('.SEODescription').find('.wordsCount').html(data.data.wordsCount);
                $('.SEODescription').find('.sentencesCount').html(data.data.sentencesCount);
            },
            error: function (jqXhr, textStatus, errorMessage) {

                responseError(jqXhr, textStatus, errorMessage)
            }
        });

    });
    var metaTitle=1;
    $('#metaTitle').on('change',function () {
        var formData = new FormData();
        var text = $('#metaTitle').val();
        formData.append('text', text);
        $.ajax(base_url + 'admin/text', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data, status, xhr) {
                // $('.SEODetails').fadeIn();
                if(metaTitle===1) {
                    $('#metaTitle').after('<div class="text-white my-2 SEODetails SEOTitle">\n' +
                        '                            <span>Readability: <span class="readability"></span></span>\n' +
                        '                            <span>Text Length: <span class="textLength"></span></span>\n' +
                        '                            <span>Letters Count: <span class="lettersCount"></span></span>\n' +
                        '                            <span>Words Count: <span class="wordsCount"></span></span>\n' +
                        '                            <span>Sentences Count: <span class="sentencesCount"></span></span>\n' +
                        '                        </div>');
                }
                metaTitle=2;
                $('.SEOTitle').find('.readability').html(data.data.readability);
                $('.SEOTitle').find('.textLength').html(data.data.textLength);
                $('.SEOTitle').find('.lettersCount').html(data.data.lettersCount);
                $('.SEOTitle').find('.wordsCount').html(data.data.wordsCount);
                $('.SEOTitle').find('.sentencesCount').html(data.data.sentencesCount);
            },
            error: function (jqXhr, textStatus, errorMessage) {

                responseError(jqXhr, textStatus, errorMessage)
            }
        });

    });
    var metaKeywords=1;
    $('#metaKeywords').on('change',function () {
        var formData = new FormData();
        var text = $('#metaKeywords').val();
        formData.append('text', text);
        $.ajax(base_url + 'admin/text', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data, status, xhr) {
                // $('.SEODetails').fadeIn();
                if(metaKeywords===1) {
                    $('#metaKeywords').after('<div class="text-white my-2 SEODetails SEOKey">\n' +
                        '                            <span>Readability: <span class="readability"></span></span>\n' +
                        '                            <span>Text Length: <span class="textLength"></span></span>\n' +
                        '                            <span>Letters Count: <span class="lettersCount"></span></span>\n' +
                        '                            <span>Words Count: <span class="wordsCount"></span></span>\n' +
                        '                            <span>Sentences Count: <span class="sentencesCount"></span></span>\n' +
                        '                        </div>');
                }
                metaKeywords=2;
                $('.SEOKey').find('.readability').html(data.data.readability);
                $('.SEOKey').find('.textLength').html(data.data.textLength);
                $('.SEOKey').find('.lettersCount').html(data.data.lettersCount);
                $('.SEOKey').find('.wordsCount').html(data.data.wordsCount);
                $('.SEOKey').find('.sentencesCount').html(data.data.sentencesCount);
            },
            error: function (jqXhr, textStatus, errorMessage) {

                responseError(jqXhr, textStatus, errorMessage)
            }
        });

    });



});
let btn = document.querySelector("#btn");
let sidebar = document.querySelector(".sidebar");

btn.onclick = function(){
    sidebar.classList.toggle("active");
};

