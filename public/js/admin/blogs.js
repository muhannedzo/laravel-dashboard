$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });


    $('.submit').on('click',function () {
        if($('#createForm').parsley().validate()){
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var titleEn = $('#titleEn').val();
            var titleAr = $('#titleAr').val();
            var slug = $('#slug').val();
            var descriptionEn = $('#descriptionEn').val();
            var descriptionAr = $('#descriptionAr').val();
            var metaTitle = $('#metaTitle').val();
            var metaKeywords = $('#metaKeywords').val();
            var metaDescription = $('#metaDescription').val();
            var metaImg = $('#metaImg')[0].files[0];
            var cover = $('#cover')[0].files[0];
            formData.append('cover', cover);
            formData.append('titleEn', titleEn);
            formData.append('titleAr', titleAr);
            formData.append('slug', slug);
            formData.append('descriptionEn', descriptionEn);
            formData.append('descriptionAr', descriptionAr);
            formData.append('meta_image', metaImg);
            formData.append('meta_title', metaTitle);
            formData.append('meta_keywords', metaKeywords);
            formData.append('meta_description', metaDescription);

            $.ajax(base_url+'admin/blogs', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url+'admin/get-blogs');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }



    });
    $('.submitEdit').on('click',function () {
        if($('#editForm').parsley().validate()){
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var id = $('#id').val();
            var titleEn = $('#titleEn').val();
            var titleAr = $('#titleAr').val();
            var slug = $('#slug').val();
            var descriptionEn = $('#descriptionEn').val();
            var descriptionAr = $('#descriptionAr').val();
            var metaTitle = $('#metaTitle').val();
            var metaKeywords = $('#metaKeywords').val();
            var metaDescription = $('#metaDescription').val();
            var metaImg = $('#metaImg')[0].files[0];
            var cover = $('#cover')[0].files[0];

            formData.append('id', id);
            formData.append('cover', cover);
            formData.append('titleEn', titleEn);
            formData.append('titleAr', titleAr);
            formData.append('slug', slug);
            formData.append('descriptionEn', descriptionEn);
            formData.append('descriptionAr', descriptionAr);
            formData.append('meta_image', metaImg);
            formData.append('meta_title', metaTitle);
            formData.append('meta_keywords', metaKeywords);
            formData.append('meta_description', metaDescription);
            $.ajax(base_url+'admin/edit-blog', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url+'admin/get-blogs');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }



    });



    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        sortable: true,
        ajax: {
            url: base_url+'admin/blogs',
            data: function (d) {
                    d.search = $('input[type="search"]').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title_en', name: 'title'},
            {data: 'cover', "render": function (data, type, row, meta) {

                    return '<div class="text-center"><img src="' + base_url + data + '" class="w-25"></div>';
                }},
            {
                data: 'action', "render": function (data, type, row, meta) {

                    return '' +
                        '<div class="text-center"><div class="btn-group">' +
                        '<a class="edit btn btn-primary " href="edit-blog/' + row.id + '"><i class="fa fa-edit text-white"></i></a>' +

                        '<button class="delete btn btn-danger " data-id="' + row.id + '"><i class="fa fa-trash"></i></button>' +

                        '</div></div>';
                },searchable: false
            },
        ]
    });


    $(document).on('click', '.delete', function () {

        var id = $(this).data('id');
        Swal.fire({
            title: 'Delete!',
            icon: 'error',
            customClass: {
                confirmButton: 'btn btn-danger mx-3',
                cancelButton: 'btn btn-primary mx-3'
            },
            backdrop: `rgba(0, 0, 0, 0.4)`,
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            preConfirm: () => {

                $.ajax(base_url+'admin/blogs', {
                    type: 'DELETE',  // http method
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        id: id
                    },  // data to submit
                    success: function (data, status, xhr) {
                        if (data['code'] == '1') {
                            table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: data["message"]
                            });
                        } else {
                            Toast.fire({
                                icon: 'warning',
                                title: data["message"]
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        responseError(jqXhr, textStatus, errorMessage)
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });
    if($('#cover').data('value')!=undefined){
        $("#cover").fileinput({
            'showUpload': false,
            initialPreview: [
                base_url+$('#cover').data('value')
            ],
            fileActionSettings: {
                showRemove: false,
            },
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreviewFileType: 'image',
            initialPreviewConfig: [
                {caption:  base_url+$('#brandImg').data('value')},],

        });
    }else {
        $("#cover").fileinput({
            'showUpload': false,
        });
    }
    $('#cover').on('fileclear', function(event) {
        $(this).attr('required','required');
    });
    $('#descriptionEn,#descriptionAr').trumbowyg();
    $('#titleEn').on('change', function () {
        var formData = new FormData();
        var title = $('#titleEn').val();
        formData.append('action', 'blog');
        formData.append('title', title);
        $.ajax(base_url + 'admin/slug', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data, status, xhr) {
                $('#slug').val(data.data);
            },
            error: function (jqXhr, textStatus, errorMessage) {

                responseError(jqXhr, textStatus, errorMessage)
            }
        });



    });
    if($('#metaImg').data('value')!=undefined){
        $("#metaImg").fileinput({
            'showUpload': false,
            initialPreview: [
                base_url+$('#metaImg').data('value')
            ],
            fileActionSettings: {
                showRemove: false,
            },
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreviewFileType: 'image',
            initialPreviewConfig: [
                {caption:  base_url+$('#metaImg').data('value')},],

        });
    }else {
        $("#metaImg").fileinput({
            'showUpload': false,
        });
    }
    $('#metaImg').on('fileclear', function(event) {
        $(this).attr('required','required');
    });
    $('#metaDescription').trumbowyg();
});
