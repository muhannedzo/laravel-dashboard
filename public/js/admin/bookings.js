$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        sortable: true,
        ajax: {
            url: base_url+'admin/bookings',
            data: function (d) {
                    d.search = $('input[type="search"]').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'invoice_id', name: 'invoice_id'},
            {data: 'rental_number', name: 'Rental Number'},
            {data: 'rental_type', name: 'Rental Type'},
            {data: 'from_date', name: 'From',width:'80'},
            {data: 'to_date', name: 'To',width:'80'},
            {data: 'customer_name', name: 'Customer Name'},
            {data: 'customer_phone', name: 'Customer Phone'},
            {data: 'payment_method', name: 'Payment'},
            {
                data: 'has_babyseat', "render": function (data, type, row, meta) {
                    state=['Not Include','Include'];
                    color=['badge-danger','badge-success'];
                    return '<span class="badge '+color[data]+'">'+state[data]+'</span>';
                },
            },
            {
                data: 'has_driver', "render": function (data, type, row, meta) {
                    state=['Not Include','Include'];
                    color=['badge-danger','badge-success'];
                    return '<span class="badge '+color[data]+'">'+state[data]+'</span>';
                },
            },
            {data: 'total_price', name: 'Total'},
            {
                data: 'state', "render": function (data, type, row, meta) {
                    state=['Pending','In Process'];
                    color=['badge-danger','badge-info'];
                    return '<span class="badge '+color[data]+'">'+state[data]+'</span>';
                },
            },
            {
                data: 'action', "render": function (data, type, row, meta) {

                    return '' +
                        '<div class="text-center"><div class="btn-group">' +

                        '<button class="delete btn btn-danger " data-id="' + row.id + '"><i class="fa fa-trash"></i></button>' +
                        '<a class="btn btn-info" href="info-booking/' + row.id + '" data-id="' + row.id + '"><i class="fa fa-info-circle text-white"></i></a>' +

                        '</div></div>';
                },sortable: false
            },
        ]
    });


    $(document).on('click', '.delete', function () {

        var id = $(this).data('id');
        Swal.fire({
            title: 'Delete!',
            icon: 'error',
            customClass: {
                confirmButton: 'btn btn-danger mx-3',
                cancelButton: 'btn btn-primary mx-3'
            },
            backdrop: `rgba(0, 0, 0, 0.4)`,
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            preConfirm: () => {

                $.ajax(base_url+'admin/bookings', {
                    type: 'DELETE',  // http method
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        id: id
                    },  // data to submit
                    success: function (data, status, xhr) {
                        if (data['code'] == '1') {
                            table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: data["message"]
                            });
                        } else {
                            Toast.fire({
                                icon: 'warning',
                                title: data["message"]
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        responseError(jqXhr, textStatus, errorMessage)
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });

});
