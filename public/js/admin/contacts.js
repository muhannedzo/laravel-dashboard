$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
var i=1;
$('.plus').on('click',function () {
    i++;
    $('.contact').append('<div class="divider  my-4"></div>     <div class="form-group">\n' +
        '                            <label class="text-white" for="type'+i+'">Type</label>\n' +
        '                            <select  class="form-control" name="type'+i+'" id="type'+i+'" required>\n' +
        '                                <option disabled="disabled" selected>Type...</option>\n' +
        '                                <option value="phone">phone</option>\n' +
        '                                <option value="mobile">mobile</option>\n' +
        '                                <option value="mail">mail</option>\n' +
        '                                <option value="whatsapp">whatsapp</option>\n' +
        '                                <option value="url">url</option>\n' +
        '                                <option value="address">address</option>\n' +
        '                            </select>\n' +
        '                        </div>\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="text-white" for="title'+i+'">Title </label>\n' +
        '                            <input type="text" class="form-control" name="title'+i+'" id="title'+i+'" placeholder="Enter Title"  required>\n' +
        '                        </div>\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="text-white" for="icon'+i+'">Font Awesome Icon</label>\n' +
        '                            <input type="text" class="form-control" name="icon'+i+'" id="icon'+i+'" placeholder="Font Awesome Icon" required >\n' +
        '                        </div>\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="text-white" for="url'+i+'">URL</label>\n' +
        '                            <input type="text" class="form-control" name="url'+i+'" id="url'+i+'" placeholder="URL" required>\n' +
        '                        </div>');
});
    $('.submit').on('click',function () {
        if($('#createForm').parsley().validate()){
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var type1 = $('#type1').val();
            var title1 = $('#title1').val();
            var icon1 = $('#icon1').val();
            var url1 = $('#url1').val();
            formData.append('i', i);
            formData.append('icon1', icon1);
            formData.append('url1', url1);
            formData.append('type1', type1);
            formData.append('title1', title1);
            for(let x=2;x<=i;x++){
                formData.append('icon'+x, $('#icon'+x).val());
                formData.append('url'+x, $('#url'+x).val());
                formData.append('type'+x, $('#type'+x).val());
                formData.append('title'+x, $('#title'+x).val());
            }
            $.ajax(base_url+'admin/contacts', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url+'admin/get-contacts');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }



    });
    $('.submitEdit').on('click',function () {
        if($('#editForm').parsley().validate()){
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var id = $('#id').val();
            var icon = $('#icon').val();
            var url = $('#url').val();
            var type = $('#type').val();
            var title = $('#title').val();
            formData.append('id', id);
            formData.append('icon', icon);
            formData.append('url', url);
            formData.append('type', type);
            formData.append('title', title);
            $.ajax(base_url+'admin/edit-contact', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url+'admin/get-contacts');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }



    });



    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        sortable: true,
        ajax: {
            url: base_url+'admin/contacts',
            data: function (d) {
                    d.search = $('input[type="search"]').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'type', name: 'type'},
            {data: 'title', name: 'Title'},
            {
                data: 'icon', "render": function (data, type, row, meta) {

                    return '' +
                        '<i class="fs-20 fa fa-'+data+'"></i>' ;
                },searchable: false
            },
            {
                data: 'url', "render": function (data, type, row, meta) {

                    return '' +
                        '<a class="text-white" href="'+data+'">'+data+'</a>' ;
                },searchable: false
            },
            {
                data: 'action', "render": function (data, type, row, meta) {

                    return '' +
                        '<div class="text-center"><div class="btn-group">' +
                        '<a class="edit btn btn-primary " href="edit-contact/' + row.id + '"><i class="fa fa-edit text-white"></i></a>' +

                        '<button class="delete btn btn-danger " data-id="' + row.id + '"><i class="fa fa-trash"></i></button>' +

                        '</div></div>';
                },searchable: false
            },
        ]
    });


    $(document).on('click', '.delete', function () {

        var id = $(this).data('id');
        Swal.fire({
            title: 'Delete!',
            icon: 'error',
            customClass: {
                confirmButton: 'btn btn-danger mx-3',
                cancelButton: 'btn btn-primary mx-3'
            },
            backdrop: `rgba(0, 0, 0, 0.4)`,
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            preConfirm: () => {

                $.ajax(base_url+'admin/contacts', {
                    type: 'DELETE',  // http method
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        id: id
                    },  // data to submit
                    success: function (data, status, xhr) {
                        if (data['code'] == '1') {
                            table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: data["message"]
                            });
                        } else {
                            Toast.fire({
                                icon: 'warning',
                                title: data["message"]
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        responseError(jqXhr, textStatus, errorMessage)
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });


});
