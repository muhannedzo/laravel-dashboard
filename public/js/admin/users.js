$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    $('#createUserForm').submit(function (e) {
        e.preventDefault();
        var formData = new FormData();
        var name = $('#name').val();
        var role = $('#role').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var img = $('#avatar')[0].files[0];
        formData.append('avatar', img);
        formData.append('name', name);
        formData.append('role', role);
        formData.append('email', email);
        formData.append('password', password);
        $.ajax(base_url+'admin/users', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data, status, xhr) {
                if (data['code'] == '1') {
                    Toast.fire({
                        icon: 'success',
                        title: data["message"]
                    });
                    window.location.replace(base_url+'admin');
                } else {
                    Toast.fire({
                        icon: 'warning',
                        title: data["message"]
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                responseError(jqXhr, textStatus, errorMessage)
            }
        });

    });
    $('#logInForm').submit(function (e) {
        e.preventDefault();

        var email = $('#email').val();
        var password = $('#password').val();
        $.ajax(base_url+'login', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                email: email,
                password: password
            },  // data to submit
            success: function (data, status, xhr) {
                if (data['code'] == '1') {
                    Toast.fire({
                        icon: 'success',
                        title: data["message"]
                    });
                    window.location.replace('../')
                } else {
                    Toast.fire({
                        icon: 'warning',
                        title: data["message"]
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                responseError(jqXhr, textStatus, errorMessage)
            }
        });

    });
    $('#updateForm').submit(function (e) {
        e.preventDefault();
        var formData = new FormData();
        var name = $('#name').val();
        var email = $('#email').val();
        var role = $('#role').val();
        var id = $('#id').val();
        var img = $('#avatar')[0].files[0];
        formData.append('avatar', img);
        formData.append('name', name);
        formData.append('role', role);
        formData.append('email', email);
        formData.append('id', id);
        $.ajax(base_url+'admin/edit-user', {
            type: 'POST',  // http method
            dataType: "json",
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,  // data to submit
            success: function (data, status, xhr) {
                if (data['code'] == '1') {
                    Toast.fire({
                        icon: 'success',
                        title: data["message"]
                    });
                    setTimeout(function () {
                        window.location.replace('../')
                    }, 3000)
                } else {
                    Toast.fire({
                        icon: 'warning',
                        title: data["message"]
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                responseError(jqXhr, textStatus, errorMessage)
            }
        });

    });

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        sortable: true,
        ajax: {
            url: base_url+'admin/users',
            data: function (d) {
                d.stateFilter = $('#stateFilter').val(),
                    d.search = $('input[type="search"]').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'user_name', name: 'user_name'},
            {data: 'role_name', name: 'role_name'},
            {
                data: 'avatar', "render": function (data, type, row, meta) {

                    return '<div class="text-center"><img src="' + base_url + data + '" class="table-img w-50"></div>'
                },
                width:100
            },
            {data: 'email', name: 'email'},
            {
                data: 'state', "render": function (data, type, row, meta) {
                    var state = ['info', 'success', 'danger'];
                    var stateDb = ['unset', 'Active', 'Unactivated'];
                    return '<span class="badge badge-pill badge-' + state[row.state] + '">' + stateDb[row.state] + '</span>'
                }
            },
            {
                data: 'action', "render": function (data, type, row, meta) {
                    var state = ['info', 'danger', 'success'];
                    var stateIcon = ['info', 'eye-slash', 'eye'];
                    var stateDb = ['unset', 'Unactivated', 'Active'];
                    return '<div class="text-center"><div class="btn-group">' +
                        '<button class="changeState btn btn-' + state[row.state] + ' " data-state="' + row.state + '"  data-id="' + row.id + '" title="' + stateDb[row.state] + '"><i class="fa fa-' + stateIcon[row.state] + '"></i></button>' +
                        '<a class="edit btn btn-primary " href="user-edit/' + row.id + '"><i class="fa fa-edit text-white"></i></a>' +

                        '<button class="delete btn btn-danger " data-id="' + row.id + '"><i class="fa fa-trash"></i></button>' +

                        '</div></div>';
                },searchable: false
            },
        ]
    });
    $('#stateFilter').change(function () {
        table.draw();
    });

    $(document).on('click', '.delete', function () {

        var id = $(this).data('id');
        Swal.fire({
            title: 'Delete!',
            icon: 'error',
            customClass: {
                confirmButton: 'btn btn-danger mx-3',
                cancelButton: 'btn btn-primary mx-3'
            },
            backdrop: `rgba(0, 0, 0, 0.4)`,
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            preConfirm: () => {

                $.ajax(base_url+'admin/users', {
                    type: 'DELETE',  // http method
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        id: id
                    },  // data to submit
                    success: function (data, status, xhr) {
                        if (data['code'] == '1') {
                            table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: data["message"]
                            });
                        } else {
                            Toast.fire({
                                icon: 'warning',
                                title: data["message"]
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        responseError(jqXhr, textStatus, errorMessage)
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });
    $(document).on('click', '.changeState', function () {

        var id = $(this).data('id');
        var state = $(this).data('state');
        if (state === 1) {
            state = 2;
        } else {
            state = 1
        }
        Swal.fire({
            title: 'Change State',
            icon: 'warning',
            customClass: {
                confirmButton: 'btn btn-success mx-3',
                cancelButton: 'btn btn-primary mx-3'
            },
            backdrop: `rgba(0, 0, 0, 0.4)`,
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Change',
            showLoaderOnConfirm: true,
            preConfirm: () => {

                $.ajax(base_url+'admin/users', {
                    type: 'PUT',  // http method
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        id: id,
                        state: state
                    },  // data to submit
                    success: function (data, status, xhr) {
                        if (data['code'] == '1') {
                            table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: data["message"]
                            });
                        } else {
                            Toast.fire({
                                icon: 'warning',
                                title: data["message"]
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        responseError(jqXhr, textStatus, errorMessage)
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });
});
