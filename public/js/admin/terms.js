$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
var i=1;
$('.plus').on('click',function () {
    i++;
    $('.term').append('<div class="divider  my-4"></div>  <div class="form-group">\n' +
        '                            <label class="text-white" for="titleEn'+i+'">Title En</label>\n' +
        '                            <input type="text" class="form-control" name="titleEn'+i+'" id="titleEn'+i+'" placeholder="Title En"  >\n' +
        '                        </div>\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="text-white" for="titleAr'+i+'">Title Ar</label>\n' +
        '                            <input type="text" class="form-control" name="titleAr'+i+'" id="titleAr'+i+'" placeholder="Title Ar"  >\n' +
        '                        </div>\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="text-white" for="descriptionEn'+i+'">Description En</label>\n' +
        '                            <textarea  class="form-control textArea" name="descriptionEn'+i+'" id="descriptionEn'+i+'" placeholder="Description En"  ></textarea>\n' +
        '                        </div>\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="text-white" for="descriptionAr'+i+'">Description Ar</label>\n' +
        '                            <textarea  class="form-control textArea" name="descriptionAr'+i+'" id="descriptionAr'+i+'" placeholder="Description Ar"  ></textarea>\n' +
        '                        </div>');
    $('.textArea').trumbowyg();
});
    $('.submit').on('click',function () {
        if($('#createForm').parsley().validate()){
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var titleEn1 = $('#titleEn1').val();
            var titleAr1 = $('#titleAr1').val();
            var descriptionEn1 = $('#descriptionEn1').val();
            var descriptionAr1 = $('#descriptionAr1').val();
            formData.append('i', i);
            formData.append('titleEn1', titleEn1);
            formData.append('titleAr1', titleAr1);
            formData.append('descriptionEn1', descriptionEn1);
            formData.append('descriptionAr1', descriptionAr1);
            for(let x=2;x<=i;x++){
                formData.append('titleEn'+x, $('#titleEn'+x).val());
                formData.append('titleAr'+x, $('#titleAr'+x).val());
                formData.append('descriptionEn'+x, $('#descriptionEn'+x).val());
                formData.append('descriptionAr'+x, $('#descriptionAr'+x).val());
            }
            $.ajax(base_url+'admin/terms', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url+'admin/get-terms');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }



    });
    $('.submitEdit').on('click',function () {
        if($('#editForm').parsley().validate()){
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var id = $('#id').val();
            var titleEn = $('#titleEn').val();
            var titleAr = $('#titleAr').val();
            var descriptionEn = $('#descriptionEn').val();
            var descriptionAr = $('#descriptionAr').val();
            formData.append('id', id);
            formData.append('titleEn', titleEn);
            formData.append('titleAr', titleAr);
            formData.append('descriptionEn', descriptionEn);
            formData.append('descriptionAr', descriptionAr);
            $.ajax(base_url+'admin/edit-term', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url+'admin/get-terms');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }



    });



    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        sortable: true,
        ajax: {
            url: base_url+'admin/terms',
            data: function (d) {
                    d.search = $('input[type="search"]').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title_en', name: 'Title'},
            {
                data: 'action', "render": function (data, type, row, meta) {

                    return '' +
                        '<div class="text-center"><div class="btn-group">' +
                        '<a class="edit btn btn-primary " href="edit-term/' + row.id + '"><i class="fa fa-edit text-white"></i></a>' +

                        '<button class="delete btn btn-danger " data-id="' + row.id + '"><i class="fa fa-trash"></i></button>' +

                        '</div></div>';
                },searchable: false
            },
        ]
    });


    $(document).on('click', '.delete', function () {

        var id = $(this).data('id');
        Swal.fire({
            title: 'Delete!',
            icon: 'error',
            customClass: {
                confirmButton: 'btn btn-danger mx-3',
                cancelButton: 'btn btn-primary mx-3'
            },
            backdrop: `rgba(0, 0, 0, 0.4)`,
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            preConfirm: () => {

                $.ajax(base_url+'admin/terms', {
                    type: 'DELETE',  // http method
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        id: id
                    },  // data to submit
                    success: function (data, status, xhr) {
                        if (data['code'] == '1') {
                            table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: data["message"]
                            });
                        } else {
                            Toast.fire({
                                icon: 'warning',
                                title: data["message"]
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        responseError(jqXhr, textStatus, errorMessage)
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });

    $('.textArea').trumbowyg();

});
