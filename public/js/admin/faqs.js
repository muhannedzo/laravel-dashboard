$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
var i=1;
$('.plus').on('click',function () {
    i++;
    $('.faq').append('<div class="divider  my-4"></div> <div class="form-group">\n' +
        '                             <label class="text-white" for="q'+i+'En">Question En</label>\n' +
        '                            <input type="text" class="form-control" name="q'+i+'En" id="q'+i+'En" placeholder="Question En"  >\n' +
        '                        </div>\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="text-white" for="q'+i+'Ar">Question Ar</label>\n' +
        '                            <input type="text" class="form-control" name="q'+i+'Ar" id="q'+i+'Ar" placeholder="Question Ar"  >\n' +
        '                        </div>\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="text-white" for="a'+i+'En">Answer En</label>\n' +
        '                            <input type="text" class="form-control" name="a'+i+'En" id="a'+i+'En" placeholder="Answer En"  >\n' +
        '                        </div>\n' +
        '                        <div class="form-group">\n' +
        '                            <label class="text-white" for="a'+i+'Ar">Answer Ar</label>\n' +
        '                            <input type="text" class="form-control" name="a'+i+'Ar" id="a'+i+'Ar" placeholder="Answer Ar"  >\n' +
        '                        </div>')
});
    $('.submit').on('click',function () {
        if($('#createForm').parsley().validate()){
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var q1En = $('#q1En').val();
            var q1Ar = $('#q1Ar').val();
            var a1En = $('#a1En').val();
            var a1Ar = $('#a1Ar').val();
            formData.append('i', i);
            formData.append('q1En', q1En);
            formData.append('q1Ar', q1Ar);
            formData.append('a1En', a1En);
            formData.append('a1Ar', a1Ar);
            for(let x=2;x<=i;x++){
                formData.append('q'+x+'En', $('#q'+x+'En').val());
                formData.append('q'+x+'Ar', $('#q'+x+'Ar').val());
                formData.append('a'+x+'En', $('#a'+x+'En').val());
                formData.append('a'+x+'Ar', $('#a'+x+'Ar').val());
            }
            $.ajax(base_url+'admin/faqs', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url+'admin/get-faqs');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }



    });
    $('.submitEdit').on('click',function () {
        if($('#editForm').parsley().validate()){
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var id = $('#id').val();
            var qEn = $('#qEn').val();
            var qAr = $('#qAr').val();
            var aEn = $('#aEn').val();
            var aAr = $('#aAr').val();

            formData.append('id', id);
            formData.append('qEn', qEn);
            formData.append('qAr', qAr);
            formData.append('aEn', aEn);
            formData.append('aAr', aAr);
            $.ajax(base_url+'admin/edit-faq', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url+'admin/get-faqs');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }



    });



    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        sortable: true,
        ajax: {
            url: base_url+'admin/faqs',
            data: function (d) {
                    d.search = $('input[type="search"]').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'question_en', name: 'Question'},
            {data: 'answer_en', name: 'Answer'},
            {
                data: 'action', "render": function (data, type, row, meta) {

                    return '' +
                        '<div class="text-center"><div class="btn-group">' +
                        '<a class="edit btn btn-primary " href="edit-faq/' + row.id + '"><i class="fa fa-edit text-white"></i></a>' +

                        '<button class="delete btn btn-danger " data-id="' + row.id + '"><i class="fa fa-trash"></i></button>' +

                        '</div></div>';
                },searchable: false
            },
        ]
    });


    $(document).on('click', '.delete', function () {

        var id = $(this).data('id');
        Swal.fire({
            title: 'Delete!',
            icon: 'error',
            customClass: {
                confirmButton: 'btn btn-danger mx-3',
                cancelButton: 'btn btn-primary mx-3'
            },
            backdrop: `rgba(0, 0, 0, 0.4)`,
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            preConfirm: () => {

                $.ajax(base_url+'admin/faqs', {
                    type: 'DELETE',  // http method
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        id: id
                    },  // data to submit
                    success: function (data, status, xhr) {
                        if (data['code'] == '1') {
                            table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: data["message"]
                            });
                        } else {
                            Toast.fire({
                                icon: 'warning',
                                title: data["message"]
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        responseError(jqXhr, textStatus, errorMessage)
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });


});
