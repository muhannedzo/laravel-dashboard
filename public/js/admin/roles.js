$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }

    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    $('#createRoleForm').submit(function (e) {
        e.preventDefault();
        var formData = new FormData();
        var name = $('#name').val();
        var permissions=[];
         $('.permission:checked').each(function (i) {
             permissions[i]=$(this).val();
         });

        formData.append('name', name);
        formData.append('permissions', permissions);
        $.ajax(base_url + 'admin/roles', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data, status, xhr) {
                if (data['code'] == '1') {
                    Toast.fire({
                        icon: 'success',
                        title: data["message"]
                    });
                    window.location.replace(base_url+'admin/all-roles');
                } else {
                    Toast.fire({
                        icon: 'warning',
                        title: data["message"]
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                responseError(jqXhr, textStatus, errorMessage)
            }
        });


    });

    $('#updateForm').submit(function (e) {
        e.preventDefault();

        var formData = new FormData();
        var id = $('#id').val();
        var permissions=[];
        $('.permission:checked').each(function (i) {
            permissions[i]=$(this).val();
        });

        formData.append('permissions', permissions);
        formData.append('id', id);

        $.ajax(base_url +'admin/rolesEdit', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,  // data to submit
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data, status, xhr) {
                if (data['code'] == '1') {
                    Toast.fire({
                        icon: 'success',
                        title: data["message"]
                    });
                    setTimeout(function () {
                        window.location.replace(base_url+'admin/all-roles')
                    }, 3000)
                } else {
                    Toast.fire({
                        icon: 'warning',
                        title: data["message"]
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                responseError(jqXhr, textStatus, errorMessage)
            }
        });

    });

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        sortable: true,
        ajax: {
            url: base_url + 'admin/roles',
            data: function (d) {
                d.search = $('input[type="search"]').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {
                data: 'action', "render": function (data, type, row, meta) {
                    return '<div class="text-center"><div class="btn-group">' +
                        '<a class="edit btn btn-primary " href="edit-role/' + row.id + '"><i class="fa fa-edit text-white"></i></a>' +
                        '<button class="delete btn btn-danger " data-id="' + row.id + '"><i class="fa fa-trash"></i></button>' +

                        '</div></div>';
                }, searchable: false
            },
        ]
    });

    $(document).on('click', '.delete', function () {

        var id = $(this).data('id');
        Swal.fire({
            title: 'Delete!',
            icon: 'error',
            customClass: {
                confirmButton: 'btn btn-danger mx-3',
                cancelButton: 'btn btn-primary mx-3'
            },
            backdrop: `rgba(0, 0, 0, 0.4)`,
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            preConfirm: () => {

                $.ajax(base_url+'admin/role', {
                    type: 'DELETE',  // http method
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        id: id
                    },  // data to submit
                    success: function (data, status, xhr) {
                        if (data['code'] == '1') {
                            table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: data["message"]
                            });
                        } else {
                            Toast.fire({
                                icon: 'warning',
                                title: data["message"]
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        responseError(jqXhr, textStatus, errorMessage)
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });
});
