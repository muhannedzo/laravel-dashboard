$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }

    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    $('#logInForm').submit(function (e) {
        e.preventDefault();

        var email = $('#email').val();
        var password = $('#password').val();
        $.ajax('../login', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                email: email,
                password: password
            },  // data to submit
            success: function (data, status, xhr) {
                if (data['code'] == '1') {
                    Toast.fire({
                        icon: 'success',
                        title: data["message"]
                    });
                    window.location.replace(base_url+'admin')
                } else {
                    Toast.fire({
                        icon: 'warning',
                        title: data["message"]
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                responseError(jqXhr, textStatus, errorMessage)
            }
        });

    });

});
