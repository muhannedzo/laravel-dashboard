$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }

    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    $('#model').on('change', function () {
        $('#slugGroup').val($(this).val().split(' ').join('_'));
        var formData = new FormData();
        var title = $('#model').val();
        formData.append('action', 'car');
        formData.append('title', title);
        $.ajax(base_url + 'admin/slug', {
            type: 'POST',  // http method
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data, status, xhr) {
                $('#slug').val(data.data);
            },
            error: function (jqXhr, textStatus, errorMessage) {

                responseError(jqXhr, textStatus, errorMessage)
            }
        });



    });
    allImagesPrev =[];
    oldImg =[];
    $('.submit').on('click', function () {
        if ($('#createForm').parsley().validate()) {
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var type = $('#type').val();
            var brand = $('#brand').val();
            var body = $('#body').val();
            var model = $('#model').val();
            var inner = $('#inner').val();
            var outer = $('#outer').val();
            var daily = $('#daily').val();
            var hourly = $('#hourly').val();
            var seats = $('#seats').val();
            var year = $('#year').val();
            var slug = $('#slug').val();
            var slugGroup = $('#slugGroup').val();
            var descriptionEn = $('#descriptionEn').val();
            var descriptionAr = $('#descriptionAr').val();
            var metaTitle = $('#metaTitle').val();
            var metaKeywords = $('#metaKeywords').val();
            var metaDescription = $('#metaDescription').val();
            var imgs = $('#imgs')[0];
            var imgsCount = $('#imgs')[0].files.length;
            var metaImg = $('#metaImg')[0].files[0];
            for (let i = 0; i < imgsCount; i++) {
                formData.append('imgs' + i, imgs.files[i]);
            }
            formData.append('meta_image', metaImg);
            formData.append('imgsCount', imgsCount);
            formData.append('type', type);
            formData.append('brand', brand);
            formData.append('body', body);
            formData.append('inner', inner);
            formData.append('outer', outer);
            formData.append('daily', daily);
            formData.append('hourly', hourly);
            formData.append('seats', seats);
            formData.append('model', model);
            formData.append('year', year);
            formData.append('slug', slug);
            formData.append('slugGroup', slugGroup);
            formData.append('descriptionEn', descriptionEn);
            formData.append('descriptionAr', descriptionAr);
            formData.append('meta_title', metaTitle);
            formData.append('meta_keywords', metaKeywords);
            formData.append('meta_description', metaDescription);
            $.ajax(base_url + 'admin/cars', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url + 'admin/get-cars');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submit').prop("disabled", false);
                    $('.submit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }


    });
    $('.submitEdit').on('click', function () {
        if ($('#editForm').parsley().validate()) {
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var id = $('#id').val();
            var type = $('#type').val();
            var brand = $('#brand').val();
            var inner = $('#inner').val();
            var outer = $('#outer').val();
            var daily = $('#daily').val();
            var hourly = $('#hourly').val();
            var seats = $('#seats').val();
            var body = $('#body').val();
            var model = $('#model').val();
            var year = $('#year').val();
            var slug = $('#slug').val();
            var slugGroup = $('#slugGroup').val();
            var descriptionEn = $('#descriptionEn').val();
            var descriptionAr = $('#descriptionAr').val();
            var metaTitle = $('#metaTitle').val();
            var metaKeywords = $('#metaKeywords').val();
            var metaDescription = $('#metaDescription').val();

            var imgs = $('#imgs')[0];
            var imgsCount = $('#imgs')[0].files.length;
            var metaImg = $('#metaImg')[0].files[0];
            var oldImgString;
            if(oldImg[0]!=undefined){
                oldImgString= oldImg.join(',');
            }
            for (let i = 0; i < imgsCount; i++) {
                formData.append('imgs' + i, imgs.files[i]);
            }
            formData.append('id', id);

            formData.append('meta_image', metaImg);
            formData.append('imgsCount', imgsCount);
            formData.append('type', type);
            formData.append('brand', brand);
            formData.append('inner', inner);
            formData.append('outer', outer);
            formData.append('daily', daily);
            formData.append('oldImg', oldImgString);
            formData.append('hourly', hourly);
            formData.append('seats', seats);
            formData.append('body', body);
            formData.append('model', model);
            formData.append('year', year);
            formData.append('slug', slug);
            formData.append('slugGroup', slugGroup);
            formData.append('descriptionEn', descriptionEn);
            formData.append('descriptionAr', descriptionAr);
            formData.append('meta_title', metaTitle);
            formData.append('meta_keywords', metaKeywords);
            formData.append('meta_description', metaDescription);
            $.ajax(base_url + 'admin/edit-car', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.replace(base_url + 'admin/get-cars');
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }


    });


    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        sortable: true,
        ajax: {
            url: base_url + 'admin/cars',
            data: function (d) {
                d.search = $('input[type="search"]').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},

            {
                data: 'type', "render": function (data, type, row, meta) {
                    return '<span>' + row.types.name_en + '</span>'
                }
            },
            {data: 'model', name: 'model'},
            {
                data: 'brand', "render": function (data, type, row, meta) {
                    return '<span>' + row.brands.name + '</span>'
                }
            },
            {data: 'year', name: 'year'},
            {data: 'slug', name: 'slug'},
            {
                data: 'imgs', "render": function (data, type, row, meta) {
                    var img=data.split(',');
                    return '<div class="text-center"><img src="' + base_url + img[0] + '" class="w-50"></div>';
                }
            },
            {
                data: 'action', "render": function (data, type, row, meta) {

                    return '' +
                        '<div class="text-center"><div class="btn-group">' +
                        '<a class="edit btn btn-primary " href="edit-car/' + row.id + '"><i class="fa fa-edit text-white"></i></a>' +

                        '<button class="delete btn btn-danger " data-id="' + row.id + '"><i class="fa fa-trash"></i></button>' +

                        '</div></div>';
                }, searchable: false
            },
        ]
    });


    $(document).on('click', '.delete', function () {

        var id = $(this).data('id');
        Swal.fire({
            title: 'Delete!',
            icon: 'error',
            customClass: {
                confirmButton: 'btn btn-danger mx-3',
                cancelButton: 'btn btn-primary mx-3'
            },
            backdrop: `rgba(0, 0, 0, 0.4)`,
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            preConfirm: () => {

                $.ajax(base_url + 'admin/cars', {
                    type: 'DELETE',  // http method
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        id: id
                    },  // data to submit
                    success: function (data, status, xhr) {
                        if (data['code'] == '1') {
                            table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: data["message"]
                            });
                        } else {
                            Toast.fire({
                                icon: 'warning',
                                title: data["message"]
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        responseError(jqXhr, textStatus, errorMessage)
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });
    $('#brand').select2({
        placeholder: "Brand...",
    });
    $('#type').select2({
        placeholder: "Type...",
    });
    $('#body').select2({
        placeholder: "Body...",
    });


    if ($('#imgs').data('value') != undefined) {
        var allImage=$('#imgs').data('value').split(',');
        allImages =[];

        jQuery.each(allImage,function (i,val) {
            allImages[i]={'caption' : base_url +val, key: i};
            allImagesPrev[i]=base_url +val;
        });
        $("#imgs").fileinput({
            'showUpload': false,
            initialPreview: allImagesPrev,
            // fileActionSettings: {
            //     showRemove: false,
            // },
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreviewFileType: 'image',
            initialPreviewConfig: allImages,
            // overwriteInitial: false,

        });
    } else {
        $("#imgs").fileinput({
            'showUpload': false,
        });
    }

    $('#imgs').on('filesorted', function(event, params) {
        console.log('File sorted ', params.previewId, params.oldIndex, params.newIndex, params.stack);
        jQuery.each(params.stack,function (i,value) {
            oldImg[i]= value.caption.replace(base_url,'');
        });
    });

    $('#imgs').on('fileclear', function (event) {
        $(this).attr('required', 'required');
    });

    if ($('#metaImg').data('value') != undefined) {
        $("#metaImg").fileinput({
            'showUpload': false,
            initialPreview: [
                base_url + $('#metaImg').data('value')
            ],
            fileActionSettings: {
                showRemove: false,
            },
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreviewFileType: 'image',
            initialPreviewConfig: [
                {caption: base_url + $('#metaImg').data('value')},],

        });
    } else {
        $("#metaImg").fileinput({
            'showUpload': false,
        });
    }
    $('#metaImg').on('fileclear', function (event) {
        $(this).attr('required', 'required');
    });
    $('#descriptionEn,#descriptionAr,#metaDescription').trumbowyg();
});
