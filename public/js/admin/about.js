$(document).ready(function () {
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });



    $('.submitEdit').on('click',function () {
        if($('#aboutForm').parsley().validate()){
            $(this).prop("disabled", true);
            $(this).html($(this).data('loading'));
            var formData = new FormData();
            var id = $('#id').val();
            var titleEn = $('#titleEn').val();
            var titleAr = $('#titleAr').val();
            var descriptionEn = $('#descriptionEn').val();
            var descriptionAr = $('#descriptionAr').val();
            var cover = $('#cover')[0].files[0];
            formData.append('id', id);
            formData.append('cover', cover);
            formData.append('titleEn', titleEn);
            formData.append('titleAr', titleAr);
            formData.append('descriptionEn', descriptionEn);
            formData.append('descriptionAr', descriptionAr);
            $.ajax(base_url+'admin/about', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data, status, xhr) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                        window.location.reload();
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.submitEdit').prop("disabled", false);
                    $('.submitEdit').html('Save');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }



    });



    if($('#cover').data('value')!=undefined){
        $("#cover").fileinput({
            'showUpload': false,
            initialPreview: [
                base_url+$('#cover').data('value')
            ],
            fileActionSettings: {
                showRemove: false,
            },
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreviewFileType: 'image',
            initialPreviewConfig: [
                {caption:  base_url+$('#brandImg').data('value')},],

        });
    }else {
        $("#cover").fileinput({
            'showUpload': false,
        });
    }
    $('#cover').on('fileclear', function(event) {
        $(this).attr('required','required');
    });
    $('.textArea').trumbowyg();

});
