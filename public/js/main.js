$(document).ready(function () {
    // new WOW().init();
    $('.loader').remove();

    // $(".lazy").Lazy();
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }

    // menu
    // open menu
    $('.open-nav').click(function () {
        $(".nav").css('width', '100vw');
        $('.nav-div').css('display', 'contents');
    });
    // close menu
    $('.close-nav').click(function () {
        $(".nav").css('width', '0');
        $('.nav-div').css('display', 'none');
    });
    // filter
    // open filter
    $('.open-filter').click(function () {
        $(".filter").css('width', '100vw');
        $('.filter-div').css('display', 'contents');
    });
    // close menu
    $('.close-filter').click(function () {
        $(".filter").css('width', '0');
        $('.filter-div').css('display', 'none');
    });
    // filter price
    var lowPrice = 0;
    var maxPrice = 8000;
    $('#lowPrice').on('input', function () {
        lowPrice = $(this).val();
        this.value = Math.min(this.value, this.parentNode.childNodes[5].value - 1);
        var value = (100 / (parseInt(this.max) - parseInt(this.min))) * parseInt(this.value) - (100 / (parseInt(this.max) - parseInt(this.min))) * parseInt(this.min);
        var children = this.parentNode.childNodes[1].childNodes;
        children[1].style.width = value + '%';
        children[5].style.left = value + '%';
        children[7].style.left = value + '%';
        children[11].style.left = value + '%';
        children[11].childNodes[1].innerHTML = this.value;
    });
    $('#maxPrice').on('input', function () {
        maxPrice = $(this).val();
        this.value = Math.max(this.value, this.parentNode.childNodes[3].value - (-1));
        var value = (100 / (parseInt(this.max) - parseInt(this.min))) * parseInt(this.value) - (100 / (parseInt(this.max) - parseInt(this.min))) * parseInt(this.min);
        var children = this.parentNode.childNodes[1].childNodes;
        children[3].style.width = (100 - value) + '%';
        children[5].style.right = (100 - value) + '%';
        children[9].style.left = value + '%';
        children[13].style.left = value + '%';
        children[13].childNodes[1].innerHTML = this.value;
    });
    // filter rent type
    var rentType = 1;
    $('.rentTypeFilter').on('change', function () {
        rentType = $(this).val();
    });
    // filter brand
    let brandArray = [];
    var i = 0;
    $('.brand').on('click', function () {
        $(this).toggleClass('brand-active');
        var brand = $(this).data('brand');
        if (brand !== 0) {
            if (jQuery.inArray(brand, brandArray) === -1) {
                brandArray[i] = brand;
                i++;
            } else {
                brandArray.splice($.inArray(brand, brandArray), 1);
                i--;
            }
        } else {
            brandArray = [];
            i = 0;
            $('.brand').removeClass('brand-active');
            $('.brand[data-brand="0"]').addClass('brand-active');
        }
        if (i !== 0) {
            $('.brand[data-brand="0"]').removeClass('brand-active');
        } else {
            $('.brand[data-brand="0"]').addClass('brand-active');
        }
    });
    // clear filter
    $('.clear-filter').on('click', function () {
        $('.n0philter').prop('checked', false);
        $('#rentTypeDailyFilter').prop('checked', true);
        rentType = 1;
        $('.brand').removeClass('brand-active');
        $('.brand[data-brand="0"]').addClass('brand-active');
        brand = '0';
        brandArray = [];
        i = 0;
        if(document.getElementById('lowPrice')!=  undefined) {
            var childrenLow = document.getElementById('lowPrice').parentNode.childNodes[1].childNodes;
            childrenLow[1].style.width = 0 + '%';
            childrenLow[5].style.left = 0 + '%';
            childrenLow[7].style.left = 0 + '%';
            childrenLow[11].style.left = 0 + '%';
            childrenLow[11].childNodes[1].innerHTML = 0;
            $('#lowPrice').val(0);
        }
        lowPrice = 0;
        if(document.getElementById('maxPrice')!=undefined) {
            var childrenMax = document.getElementById('maxPrice').parentNode.childNodes[1].childNodes;
            childrenMax[3].style.width = 0 + '%';
            childrenMax[5].style.right = 0 + '%';
            childrenMax[9].style.left = 100 + '%';
            childrenMax[13].style.left = 100 + '%';
            childrenMax[13].childNodes[1].innerHTML = 8000;
            $('#maxPrice').val(8000);
        }
        maxPrice = 8000;
        $('#price-from').val('');
        $('#price-to').val('');
    });
    $('.filter-btn').click(function () {
        $.ajax('car/filter', {
            type: 'POST',  // http method
            dataType: "json",
            data: {
                rent_type: rentType,
                min_price: lowPrice,
                max_price: maxPrice,
                brands: JSON.stringify(brandArray),

            }, headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },  // data to submit
            success: function (data, status, xhr) {
                $('.car-result').html(data);
                $('.car-result').find('.car-slider').slick({
                    dots: true,
                    arrows: false,
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,

                });
                $(".filter").css('width', '0');
                $('.filter-div').css('display', 'none');

            },
            error: function (jqXhr, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });
    $('.search-car-input').keyup(function () {
        var search =  $(this).val();
        if(search!=='') {
            $.ajax(base_url+'car/search', {
                type: 'POST',  // http method
                dataType: "json",
                data: {
                    search: search,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data, status, xhr) {
                    console.log(data);
                    $('.search-result').html(data);
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }
    });
    $('.searchForm').on('submit',function (e) {
        e.preventDefault();

        var search =  $(this).find('.search-car-input').val();
        if(search!=='') {
            window.location.href=base_url+'search/'+search+'';
        }
    });
    $('.filter-btn-web').click(function (e) {
        e.preventDefault();
        var low = $('#price-from').val();
        if(low!==''){
           lowPrice= low;
        }
        var max = $('#price-to').val();
        if(max!==''){
           maxPrice= max;
        }
        $.ajax(base_url+'car/filter', {
            type: 'POST',  // http method
            dataType: "json",
            data: {
                rent_type: rentType,
                min_price: lowPrice,
                max_price: maxPrice,
                brands: JSON.stringify(brandArray),

            }, headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },  // data to submit
            success: function (data, status, xhr) {
                $('.car-result').html(data);
                $('.car-slider').slick({
                    dots: true,
                    arrows: false,
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,

                });
            },
            error: function (jqXhr, textStatus, errorMessage) {
                responseError(jqXhr, textStatus, errorMessage)
            }
        });
    });
    // body slider
    $('.body-slider').slick({
        lazyLoad: 'ondemand',
        centerMode: true,
        dots: false,
        arrows: false,
        infinite: true,
        swipeToSlide: true,
        slidesToShow: 6,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    dots: false,
                    arrows: false,
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },{
                breakpoint: 767,
                settings: {
                    dots: false,
                    arrows: false,
                    infinite: true,
                    slidesToShow: 3,
                }
            }
        ]
    });

    // select body
    var activeBody=$('.body-active').data('body');
    $('.body-card').on('click', function () {

        var body = $(this).data('body');
        if(body !== activeBody) {
            $('.body-card').removeClass('body-active');
            $(this).addClass('body-active');
            $('.car-selector').fadeOut();
            $('.' + body + '').delay(400).fadeIn();
            activeBody=body;
        }

    });
    // select type
    var activeType=$('.type-active').data('type')
    $('.type').on('click',function () {
        var type = $(this).data('type');
        if(type !== activeType){
        $('.type').removeClass('type-active');
        $(this).addClass('type-active');
            $('.car-selector').fadeOut();
            $('.type-' + type + '').delay(400).fadeIn();
            activeType=type;
        }

    });

    // car img slider
    $('.car-slider').slick({
        lazyLoad: 'ondemand',
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,

    });
    // book
    // open book

    var dailyPrice;
    var hourlyPrice;
    var carId;
    var carName;

    $(document).on('click', '.bookNow', function () {
        $('#dateTime').val('');
        $('#dateTime').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            minDate: moment.utc(),
            // timePicker: true,
            locale: {
                format: 'M/DD/YYYY'
            }

        }, function (start, end, label) {
            days = Math.floor((end - start) / (1000 * 60 * 60 * 24));
            // days =days+1;
            $('#dateTime').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            calculate();


        });
        $(".select2").val('').change();
        $('#name').val('');
        $('#phone').val('');
        $('#email').val('');
        $('.rentType').prop('checked', false);
        $('#rentTypeDaily').prop('checked', true);
        $('.cashType').prop('checked', false);
        $('#deliveryBook').prop('checked', true);
        $('.book-btn').attr('disabled', 'disabled');
        $("#babySet").prop("checked", false);
        $("#driver").prop("checked", false);
        $('.bookTotal').html('');
        $(".book-menu").css('width', '100vw');
        $('.book-div').css('display', 'contents');
        dailyPrice = parseInt($(this).data('daily'));
        hourlyPrice = parseInt($(this).data('hourly'));
        carId = $(this).data('carid');
        carName = $(this).data('name');
    });
    // close book
    $('.close-book').click(function () {
        $(".book-menu").css('width', '0');
        $('.book-div').css('display', 'none');
    });

    // date time
    var days;
    var startH;
    var endH;
    var startM;
    var endM;

    $('#startTime').select2({
        placeholder: "Pickup Time",
    });

    $('#endTime').select2({
        placeholder: "Pickup Time",
    });
    $('.select2,#dateTime,.book-form .rentType').on('change', function () {

        if ($('#startTime').val() != '') {
            var selectTime = $('#startTime').val();
            var tt = selectTime.split(":");
            startH = parseInt(tt[0]);
            startM = parseInt(tt[1]);
        }
        if ($('#endTime').val() != '') {
            var selectTime = $('#endTime').val();
            var tt = selectTime.split(":");
            endH = parseInt(tt[0]);
            endM = parseInt(tt[1]);
        }
        calculate();
    });
    $('#dateTime').daterangepicker({
        autoUpdateInput: false,
        opens: 'center',
        minDate: moment.utc(),
        // timePicker: true,
        locale: {
            format: 'M/DD/YYYY'
        }

    }, function (start, end, label) {
        days = Math.floor((end - start) / (1000 * 60 * 60 * 24));
        // days =days+1;
        $('#dateTime').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        calculate();


    });


    // book form
    $('.bookInput').on('input', function () {

        calculate();
    });
    $('.book-form').on('submit', function (e) {
        e.preventDefault();
        if ($(this).parsley()) {
            var rentType = $('input[name="rentType"]:checked').val();
            var payment_method = $('input[name="cashType"]:checked').val();
            var name = $('#name').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var startTime = $('#startTime').val();
            var endTime = $('#endTime').val();
            var dateTime = $('#dateTime').val();
            var babySet = $('#babySet:checked').val();
            var driver = $('#driver:checked').val();
            if (babySet != undefined) {
                babySet = 1;
            } else {
                babySet = 0;
            }
            if (driver != undefined) {
                driver = 1;
            } else {
                driver = 0;
            }
            var dateArray = dateTime.split(" - ");
            var fromDateArray = dateArray[0].split('/');
            var fromDate = fromDateArray[2] + '-' + fromDateArray[1] + '-' + fromDateArray[0] + ' ' + startTime + ':00';
            var toDateArray = dateArray[1].split('/');
            var toDate = toDateArray[2] + '-' + toDateArray[1] + '-' + toDateArray[0] + ' ' + endTime + ':00';

            $.ajax('car/book', {
                type: 'POST',  // http method
                dataType: "json",
                data: {
                    rental_type: rentType,
                    car_id: carId,
                    has_babyseat: babySet,
                    has_driver: driver,
                    from_date: fromDate,
                    to_date: toDate,
                    customer_name: name,
                    customer_phone: phone,
                    customer_email: email,
                    payment_method: payment_method,


                }, headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}, // data to submit
                success: function (data, status, xhr) {
                    $('.confirm-book').fadeIn();
                    $('.rental-num').html(data.data);
                    setTimeout(function () {
                        $('.confirm-book').fadeOut();
                        $(".book-menu").css('width', '0');
                        $('.book-div').css('display', 'none');
                    },3000)
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });


        }
    });

    $('.clear-book').click(function () {

        $('#dateTime').val('');
        $('#dateTime').daterangepicker({
            autoUpdateInput: false,
            opens: 'center',
            minDate: moment.utc(),
            // timePicker: true,
            locale: {
                format: 'M/DD/YYYY'
            }

        }, function (start, end, label) {
            days = Math.floor((end - start) / (1000 * 60 * 60 * 24));
            // days =days+1;
            $('#dateTime').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            calculate();


        });
        $(".select2").val('').change();
        $('#name').val('');
        $('#phone').val('');
        $('#email').val('');
        $('.rentType').prop('checked', false);
        $('#rentTypeDaily').prop('checked', true);
        $('.cashType').prop('checked', false);
        $('#deliveryBook').prop('checked', true);
        $('.book-btn').attr('disabled', 'disabled');
        $("#babySet").prop("checked", false);
        $("#driver").prop("checked", false);
        $('.bookTotal').html('');
    });

    $('.confirm-book').click(function () {
        $(this).fadeOut();

    });

    $('.switch_1').change(function () {
        calculate();
    });

    function calculate() {
        if ($('#name').val() != '' && $('#phone').val() != '' && $('#email').val() != '' && $('#startTime').val() != '' && $('#endTime').val() != '' && $('#dateTime').val() != '') {
            var rentType = $('input[name="rentType"]:checked').val();
            var min = endM - startM;
            var hours = endH - startH;
            var calcH = hours;
            var calcM = min;
            var calcD = days;
            var total = 0;

            if (min > 0) {
                calcH = hours + 0.5
            } else if (min < 0) {
                calcH = hours - 0.5
            }
            if (calcH < 0) {
                calcD = days - 1;
                calcH = 24 + calcH;

            }
            if (calcH > 2 || calcD > 0) {
                rentType = 'daily';

            }
            if (calcD >= 0) {
                if (rentType === 'hourly') {
                    total = calcH * hourlyPrice;

                    if ($('#babySet:checked').val() === 'babySet') {

                        total = total + 25;
                    }

                } else {

                    if (calcH > 2 && calcD !== 0) {
                        total = (calcD + 1) * dailyPrice;
                        if ($('#babySet:checked').val() === 'babySet') {

                            total = total + 25 * (calcD + 1);
                        }

                    } else {
                        if (calcD === 0) {
                            calcD = calcD + 1;
                        }
                        total = calcD * dailyPrice;
                        if ($('#babySet:checked').val() === 'babySet') {

                            total = total + (25 * calcD);
                        }
                    }
                }


                $('.bookTotal').html(total + ' AED');
                $('.book-btn').removeAttr('disabled');
            } else {
                $('.book-btn').attr('disabled', 'disabled');
                $('.bookTotal').html('Incorrect Date')
            }
        } else {
            $('.book-btn').attr('disabled', 'disabled');
        }
    }

    // details
    $('.car-slider2').slick({
        dots: false,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.car-slider3'
    });
    $('.car-slider3').slick({
        dots: false,
        arrows: false,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        asNavFor: '.car-slider2',
        centerMode: true,
        focusOnSelect: true
    });


    // about
    $(document).on('click', '.tab', function () {
        var tab = $(this).data('tab');
        $('.tab').removeClass('tab-active');
        $(this).addClass('tab-active');
        $('.tab-body').fadeOut();
        setTimeout(function () {
            $('.' + tab).fadeIn();
        }, 400);

    });


    // desktop
    var input = document.querySelector(".phone");

    $(".phone").focus(function () {
        $(this).val($(".iti__selected-dial-code").text());
    });
    if (input != null) {
        window.intlTelInput(input, {
            nationalMode: true,
            separateDialCode: true,
            preferredCountries: ['ae'],
        });
    }

    if (window.scrollY >= 20) {
        $('.navbar').addClass('dark-nav');
    } else {
        $('.navbar').removeClass('dark-nav');
    }
    $(window).scroll(function () {
        if (window.scrollY >= 20) {
            $('.navbar').addClass('dark-nav');
        } else {
            $('.navbar').removeClass('dark-nav');
        }
    });

    // var
    $(document).on('click', '.search-icon', function () {
        $('.search-input').fadeToggle();
    });
    $(".brand-slider").slick({
        lazyLoad: 'ondemand',
        useTransform: true,
        centerMode: false,
        infinite: true,
        // response:true,
        arrows: false,
        dots: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        autoplay: true,
        autoplaySpeed: 2000,
        // variableWidth: true,
        // padding:'50px',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    arrows: false,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }
            }, {
                breakpoint: 900,
                settings: {
                    arrows: false,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            }
        ]

    });
    $(".review-slider").slick({
        useTransform: true,
        centerMode: false,
        infinite: true,
        // response:true,
        arrows: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 200000,
        // variableWidth: true,
        // padding:'50px',
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]

    });

    $(document).on('submit', '#book-form', function (e) {
        e.preventDefault();
        if ($(this).parsley()) {
            $('.send-mail').html('SENDING...').attr('disabled', true);
            var name = $(this).find('#name').val();
            var email = $(this).find('#email').val();
            var phone = $(this).find('#phone').val();
            $.ajax(base_url + 'sendMail', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    name: name,
                    email: email,
                    phone: phone
                },  // data to submit
                success: function (data, status, xhr) {
                    $('.send-mail').html('SEND').removeAttr('disabled');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.send-mail').html('SEND').removeAttr('disabled');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }
    });
    $(document).on('submit', '.contact-form-web', function (e) {
        e.preventDefault();
        if ($(this).parsley()) {
            $('.send-contact-mail').html('SENDING...').attr('disabled', true);
            var name = $(this).find('#name').val();
            var email = $(this).find('#email').val();
            var phone = $(this).find('#phone').val();
            var message = $(this).find('#message').val();
            $.ajax(base_url + 'sendMail', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    name: name,
                    email: email,
                    phone: phone,
                    message: message
                },  // data to submit
                success: function (data, status, xhr) {
                    $('.send-contact-mail').html('SEND').removeAttr('disabled');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.send-contact-mail').html('SEND').removeAttr('disabled');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }
    });

});

