<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $no_of_rows = 10000;

        for( $i=0; $i < $no_of_rows; $i++ ) {
            DB::table('users')->insert([
                'name' => Str::random('10'),
                'email' => Str::random('10') . '@gmail.com',
                'password' => Hash::make('password'),
                'avatar' => 'uploads/wJx2ciU55vY8JEMDSSZAaylNIE9YFCvkEhdlCm2u.png',
            ]);
        }
//        User::factory()
//            ->count(50)
//            ->hasPosts(1)
//            ->create();
    }
}
