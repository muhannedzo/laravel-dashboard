<?php

use App\Http\Controllers\ApiHomeController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\StoriesController;
use App\Http\Controllers\SEOController;
use App\Mail\SampleMail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/storage-link', function () {
    Artisan::call('storage:link');
});

Route::get('/optimization', function () {
    Artisan::call('optimize');
    Artisan::call('config:cache');
    Artisan::call('route:cache');
});

Route::get('/clear-optimization-cash', function () {
    Artisan::call('config:clear');
    Artisan::call('route:clear');
});

Route::get('/permission-cache-reset', function () {
    Artisan::call('permission:cache-reset');
});


Route::get('admin', function () {
    if (auth()->user() && auth()->user()->state == 1) {
        return view('admin.pages.users.all-users', ['page' => 'allUsers']);
    } else {
        return view('admin.index');
    }
});


Route::group(['middleware' => ['protectedPage']], function () {
    Route::group(['middleware' => ['permission:cars manage']], function () {
//    brands

        Route::get('admin/get-brands', function () {
            return view('admin.pages.brands.all-brands', ['page' => 'allBrands']);
        })->name('allBrands');
        Route::delete('admin/brands', [VehicleController::class, 'deleteBrand']);
        Route::get('admin/brands', [VehicleController::class, 'getBrandsDatatable']);


        Route::get('admin/create-brand', function () {
            return view('admin.pages.brands.create-brand', ['page' => 'allBrands']);
        })->name('create-brand');
        Route::post('admin/brands', [VehicleController::class, 'postBrand']);


        Route::get('admin/edit-brand/{key:id}', [VehicleController::class, 'getBrandsDetails'])->name('edit-brand');
        Route::post('admin/edit-brand', [VehicleController::class, 'editBrand']);


//    type
    Route::get('admin/get-types', function () {
        return view('admin.pages.types.all-types', ['page' => 'allTypes']);
    })->name('allTypes');
    Route::delete('admin/types', [VehicleController::class, 'deleteType']);
    Route::get('admin/types', [VehicleController::class, 'getTypesDatatable']);
    Route::get('admin/create-type', function () {
        return view('admin.pages.types.create-type', ['page' => 'allTypes']);
    })->name('create-type');
    Route::post('admin/types', [VehicleController::class, 'postType']);
    Route::get('admin/edit-type/{key:id}', [VehicleController::class, 'getTypesDetails'])->name('edit-type');
    Route::post('admin/edit-type', [VehicleController::class, 'editType']);
//    bodies
    Route::get('admin/get-bodies', function () {
        return view('admin.pages.bodies.all-bodies', ['page' => 'allBodies']);
    })->name('allBodies');
    Route::delete('admin/bodies', [VehicleController::class, 'deleteBody']);
    Route::get('admin/bodies', [VehicleController::class, 'getBodiesDatatable']);
    Route::get('admin/create-body', function () {
        return view('admin.pages.bodies.create-body', ['page' => 'allBodies']);
    })->name('create-body');
    Route::post('admin/bodies', [VehicleController::class, 'postBody']);
    Route::get('admin/edit-body/{key:id}', [VehicleController::class, 'getBodiesDetails'])->name('edit-body');
    Route::post('admin/edit-body', [VehicleController::class, 'editBody']);
    //    cars
    Route::get('admin/get-cars', function () {
        return view('admin.pages.cars.all-cars', ['page' => 'allCars']);
    })->name('allCars');
    Route::delete('admin/cars', [VehicleController::class, 'deleteCar']);
    Route::get('admin/cars', [VehicleController::class, 'getCarDatatable']);
    Route::get('admin/create-car', function () {
        return view('admin.pages.cars.create-car', ['page' => 'allCars']);
    })->name('create-car');
    Route::post('admin/cars', [VehicleController::class, 'postCar']);
    Route::get('admin/edit-car/{key:id}', [VehicleController::class, 'getCarDetails'])->name('edit-car');
    Route::post('admin/edit-car', [VehicleController::class, 'editCar']);
});


    Route::group(['middleware' => ['permission:content manage']], function () {
    //    faq
    Route::get('admin/get-faqs', function () {
        return view('admin.pages.faqs.all-faqs', ['page' => 'allFaqs']);
    })->name('allFaqs');
    Route::delete('admin/faqs', [PagesController::class, 'deleteFaq']);
    Route::get('admin/faqs', [PagesController::class, 'getFaqsDatatable']);
    Route::get('admin/create-faq', function () {
        return view('admin.pages.faqs.create-faq', ['page' => 'allFaqs']);
    })->name('create-faq');
    Route::post('admin/faqs', [PagesController::class, 'postFaq']);
    Route::get('admin/edit-faq/{key:id}', [PagesController::class, 'getFaqsDetails'])->name('edit-faq');
    Route::post('admin/edit-faq', [PagesController::class, 'editFaq']);


    //    social media
    Route::get('admin/get-socials', function () {
        return view('admin.pages.socials.all-socials', ['page' => 'allSocials']);
    })->name('allSocials');
    Route::delete('admin/socials', [PagesController::class, 'deleteSocial']);
    Route::get('admin/socials', [PagesController::class, 'getSocialsDatatable']);
    Route::get('admin/create-social', function () {
        return view('admin.pages.socials.create-social', ['page' => 'allSocials']);
    })->name('create-social');
    Route::post('admin/socials', [PagesController::class, 'postSocial']);
    Route::get('admin/edit-social/{key:id}', [PagesController::class, 'getSocialsDetails'])->name('edit-social');
    Route::post('admin/edit-social', [PagesController::class, 'editSocial']);



    //    Contact
    Route::get('admin/get-contacts', function () {
        return view('admin.pages.contacts.all-contacts', ['page' => 'allContacts']);
    })->name('allContacts');
    Route::delete('admin/contacts', [PagesController::class, 'deleteContact']);
    Route::get('admin/contacts', [PagesController::class, 'getContactsDatatable']);
    Route::get('admin/create-contact', function () {
        return view('admin.pages.contacts.create-contact', ['page' => 'allContacts']);
    })->name('create-contact');
    Route::post('admin/contacts', [PagesController::class, 'postContact']);
    Route::get('admin/edit-contact/{key:id}', [PagesController::class, 'getContactsDetails'])->name('edit-contact');
    Route::post('admin/edit-contact', [PagesController::class, 'editContact']);


    //    Term
    Route::get('admin/get-terms', function () {
        return view('admin.pages.terms.all-terms', ['page' => 'allTerms']);
    })->name('allTerms');
    Route::delete('admin/terms', [PagesController::class, 'deleteTerm']);
    Route::get('admin/terms', [PagesController::class, 'getTermsDatatable']);
    Route::get('admin/create-term', function () {
        return view('admin.pages.terms.create-term', ['page' => 'allTerms']);
    })->name('create-term');
    Route::post('admin/terms', [PagesController::class, 'postTerm']);
    Route::get('admin/edit-term/{key:id}', [PagesController::class, 'getTermsDetails'])->name('edit-term');
    Route::post('admin/edit-term', [PagesController::class, 'editTerm']);


//    about

    Route::get('admin/get-about', [PagesController::class, 'getAboutDetails'])->name('getAbout');
    Route::post('admin/about', [PagesController::class, 'editAbout']);
    });

    Route::group(['middleware' => ['permission:blogs manage']], function () {
        //    blog
        Route::get('admin/get-blogs', function () {
            return view('admin.pages.blogs.all-blogs', ['page' => 'allBlogs']);
        })->name('allBlogs');
        Route::delete('admin/blogs', [PagesController::class, 'deleteBlog']);
        Route::get('admin/blogs', [PagesController::class, 'getBlogsDatatable']);
        Route::get('admin/create-blog', function () {
            return view('admin.pages.blogs.create-blog', ['page' => 'allBlogs']);
        })->name('create-blog');
        Route::post('admin/blogs', [PagesController::class, 'postBlog']);
        Route::get('admin/edit-blog/{key:id}', [PagesController::class, 'getBlogsDetails'])->name('edit-blog');
        Route::post('admin/edit-blog', [PagesController::class, 'editBlog']);
    });
    //    Booking
    Route::group(['middleware' => ['permission:bookings manage']], function () {
    Route::get('admin/get-bookings', function () {
        return view('admin.pages.bookings.all-bookings', ['page' => 'allBookings']);
    })->name('allBookings');
    Route::delete('admin/bookings', [VehicleController::class, 'deleteBooking']);
    Route::get('admin/bookings', [VehicleController::class, 'getBookingsDatatable']);
    Route::get('admin/info-booking/{key:id}', [VehicleController::class, 'getBookingDetails']);
    });

//    user
    Route::group(['middleware' => ['permission:view user']], function () {
        Route::get('admin/get-users', function () {
            return view('admin.pages.users.all-users', ['page' => 'allUsers']);
        })->name('allUsers');
        Route::delete('admin/users', [UsersController::class, 'deleteUser']);
        Route::put('admin/users', [UsersController::class, 'patchUserState']);
        Route::get('admin/users', [UsersController::class, 'getUsersDatatable']);
    });
    Route::group(['middleware' => ['permission:add user']], function () {
        Route::get('admin/create-user', function () {
            $role = UsersController::getRoles();
            return view('admin.pages.users.create-user', ['page' => 'create-user', 'roles' => $role]);
        })->name('create-user');
        Route::post('admin/users', [UsersController::class, 'postUser']);
    });
    Route::group(['middleware' => ['permission:edit user']], function () {
        Route::get('admin/user-edit/{key:id}', [UsersController::class, 'getUserDetails'])->name('edit-user');
        Route::post('admin/edit-user', [UsersController::class, 'patchUser']);
    });

//    roles
    Route::group(['middleware' => ['permission:view role']], function () {
        Route::get('admin/all-roles', function () {
            return view('admin.pages.roles.all-roles', ['page' => 'all-roles']);
        })->name('all-roles');
        Route::get('admin/roles', [RolesController::class, 'getRolesDatatable']);
    });
    Route::group(['middleware' => ['permission:add role']], function () {
        Route::get('admin/create-role', [RolesController::class, 'createRoleView'])->name('create-role');
        Route::post('admin/roles', [RolesController::class, 'postRole']);
    });
    Route::group(['middleware' => ['permission:edit role']], function () {
        Route::get('admin/edit-role/{id}', [RolesController::class, 'editRoleView'])->name('edit-role');
        Route::delete('admin/role', [RolesController::class, 'deleteRole']);
        Route::post('admin/rolesEdit/', [RolesController::class, 'patchRole']);
    });
 // stories   
    Route::group(['middleware' => ['permission:manage stories']], function () {
        Route::get('admin/all-stories', function () {
            return view('admin.pages.story.all-stories', ['page' => 'all-stories']);
        })->name('all-stories');
        Route::get('admin/stories', [StoriesController::class, 'getStoriesDatatable']);
        Route::delete('admin/stories', [StoriesController::class, 'deleteStory']);
        Route::get('admin/create-story', function () {
            return view('admin.pages.story.create-story', ['page' => 'all-stories']);
        })->name('create-story');
        Route::post('admin/stories', [StoriesController::class, 'addStory']);
        Route::get('admin/edit-story/{key:id}', [StoriesController::class, 'getStoryDetails'])->name('edit-story');
        Route::post('admin/edit-story', [StoriesController::class, 'editStory']);
    });

//    seo
    Route::Post('admin/slug',[SEOController::class,'checkSlug']);
    Route::Post('admin/text',[SEOController::class,'checkText']);
});





Route::post('login', [UsersController::class, 'logIn']);
Route::post('logout', [UsersController::class, 'logOut']);
Route::post('changeLang', [UsersController::class, 'changeLang']);


