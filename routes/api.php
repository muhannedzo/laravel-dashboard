<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\APIUsersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [APIUsersController::class, 'logIn']);
Route::group(['middleware' => ['auth:sanctum','user_state']], function () {
//    users
    Route::group(['middleware' => ['permission:view user']], function () {
        Route::get('users', [APIUsersController::class, 'getUsers']);
        Route::get('users/{key:id}', [APIUsersController::class, 'getUserById']);
    });
    Route::group(['middleware' => ['permission:add user']], function () {
        Route::post('users', [APIUsersController::class, 'addUser']);
    });
    Route::group(['middleware' => ['permission:edit user']], function () {
        Route::post('users/{id}', [APIUsersController::class, 'editUser']);
        Route::delete('users/{id}', [APIUsersController::class, 'deleteUser']);
        Route::post('userStateChange', [APIUsersController::class, 'userStateChange']);
    });
    Route::post('logout', [APIUsersController::class, 'logout']);



});
