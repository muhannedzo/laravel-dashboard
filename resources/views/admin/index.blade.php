<x-admin.header/>
<div class="container mt-5">
    <div class="row ">
        <div class="col-12 text-white text-center mt-5">
            <img src="{{asset('img/logo.svg')}}" class=" wow pulse">
            <h1 class="my-5"> LogIn</h1>
        </div>
        <div class="col-12 text-white">
            <form class="m-auto" id="logInForm" data-parsley-validate >

                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" name="email" id="email"  placeholder="Enter email" required >
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                </div>
                <button type="submit" class="btn btn-outline-light btn-block mt-4">Log In</button>
            </form>

        </div>
    </div>
</div>
<x-admin.footer/>
<script src="{{asset('js/admin/login.js')}}"></script>

