<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit Type</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="editForm" data-parsley-validate>
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group">
                        <label class="text-white" for="nameEn">Type Name En</label>
                        <input type="text" class="form-control" name="nameEn" id="nameEn" placeholder="Enter Name En"  value="{{$data['name_en']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="nameAr">Type Name Ar</label>
                        <input type="text" class="form-control" name="nameAr" id="nameAr" placeholder="Enter Name Ar" value="{{$data['name_ar']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="typeImg">Type Image</label>
                        <div class="custom-file">
                            <input id="typeImg" name="typeImg" type="file" data-parsley-errors-container="#typeImg-errors"  data-value="{{$data['img']}}" >
                        </div>
                        <div id="typeImg-errors" class="mt-1"></div>
                    </div>

                </form>
                <button type="button" class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/types.js')}}"></script>

