<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Create Term</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="createForm" data-parsley-validate>
                    <div class="term">
                        <div class="form-group">
                            <label class="text-white" for="titleEn1">Title En</label>
                            <input type="text" class="form-control" name="titleEn1" id="titleEn1" placeholder="Title En" required >
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="titleAr1">Title Ar</label>
                            <input type="text" class="form-control" name="titleAr1" id="titleAr1" placeholder="Title Ar"  required>
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="descriptionEn1">Description En</label>
                            <textarea  class="form-control textArea" name="descriptionEn1" id="descriptionEn1" placeholder="Description En"  required></textarea>
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="descriptionAr1">Description Ar</label>
                            <textarea  class="form-control textArea" name="descriptionAr1" id="descriptionAr1" placeholder="Description Ar"  required></textarea>
                        </div>
                    </div>

                </form>
                <div>
                <button class="btn btn-primary plus mt-3 mb-5"><i class="fa fa-plus"></i></button>
                </div>
                <button  class="btn btn-success submit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/terms.js')}}"></script>

