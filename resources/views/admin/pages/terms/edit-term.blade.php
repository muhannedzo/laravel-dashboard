<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit Term</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="editForm" data-parsley-validate>
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group">
                        <label class="text-white" for="titleEn">Title En</label>
                        <input type="text" class="form-control" name="titleEn" id="titleEn" placeholder="Title En" value="{{$data['title_en']}}" required >
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="titleAr">Title Ar</label>
                        <input type="text" class="form-control" name="titleAr" id="titleAr" placeholder="Title Ar" value="{{$data['title_ar']}}"  required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="descriptionEn">Description En</label>
                        <textarea  class="form-control textArea" name="descriptionEn" id="descriptionEn" placeholder="Description En"  required>{{$data['description_en']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="descriptionAr">Description Ar</label>
                        <textarea  class="form-control textArea" name="descriptionAr" id="descriptionAr" placeholder="Description Ar"   required>{{$data['description_ar']}}</textarea>
                    </div>
                </form>
                <button type="button" class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/terms.js')}}"></script>

