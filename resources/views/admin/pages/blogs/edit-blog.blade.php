<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit Blog</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="editForm" data-parsley-validate>
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group">
                        <label class="text-white" for="titleEn">Blog Title En</label>
                        <input type="text" class="form-control" name="titleEn" id="titleEn" placeholder="Enter title En" value="{{$data['title_en']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="titleAr">Blog Title Ar</label>
                        <input type="text" class="form-control" name="titleAr" id="titleAr" placeholder="Enter title Ar" value="{{$data['title_ar']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="slug">Blog Slug</label>
                        <input type="text" class="form-control" name="slug" id="slug" placeholder="Enter Slug" value="{{$data['slug']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="cover">Blog Cover</label>
                        <div class="custom-file">
                            <input id="cover" name="cover" type="file" data-parsley-errors-container="#cover-errors"  data-value="{{$data['cover']}}" >
                        </div>
                        <div id="cover-errors" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="descriptionEn">Blog Description En</label>
                        <textarea  class="form-control" name="description" id="descriptionEn" placeholder="Enter Description En"  required>{{$data['description_en']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="descriptionAr">Blog Description Ar</label>
                        <textarea  class="form-control" name="description" id="descriptionAr" placeholder="Enter Description Ar"  required>{{$data['description_ar']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaImg">SEO Meta Image</label>
                        <div class="custom-file">
                            <input type="file"  id="metaImg" name="metaImg" data-parsley-errors-container="#meta-errors"  data-value="{{$data['meta_image']}}">

                        </div>
                        <div id="meta-errors" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaTitle">SEO Meta Title</label>
                        <input type="text" class="form-control" name="metaTitle" id="metaTitle" placeholder="Enter Meta Title" value="{{$data['meta_title']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaKeywords">SEO Meta Keywords</label>
                        <input type="text" class="form-control" name="metaKeywords" id="metaKeywords" placeholder="Enter Meta Keywords" value="{{$data['meta_keywords']}}" data-role="tagsinput" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaDescription">SEO Meta Description</label>
                        <textarea type="text" class="form-control" name="metaDescription" id="metaDescription" placeholder="Meta Description...">{{$data['meta_description']}}</textarea>
                    </div>

                </form>
                <button type="button" class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/blogs.js')}}"></script>

