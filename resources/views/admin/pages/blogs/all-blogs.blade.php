<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">All Blog</h1>
                <div><a class="btn btn-success" href="{{route('create-blog')}}"><i class="fa fa-plus"></i> Create Blog</a></div>
            </div>
        </div>
    </div>
</section>
<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <table class="table table-striped table-bordered data-table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>title</th>
                        <th>cover</th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<x-admin.footer/>
<script src="{{asset('js/admin/blogs.js')}}"></script>
