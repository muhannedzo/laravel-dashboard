<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Create Blog</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="createForm" data-parsley-validate>
                    <div class="form-group">
                        <label class="text-white" for="titleEn">Blog Title En</label>
                        <input type="text" class="form-control" name="titleEn" id="titleEn" placeholder="Enter title En"  required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="titleAr">Blog Title Ar</label>
                        <input type="text" class="form-control" name="titleAr" id="titleAr" placeholder="Enter title Ar"  required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="slug">Blog Slug</label>
                        <input type="text" class="form-control" name="slug" id="slug" placeholder="Enter Slug"  required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="cover">Blog Cover</label>
                        <div class="custom-file">
                            <input id="cover" name="cover" type="file" data-parsley-errors-container="#cover-errors" required>
                        </div>
                        <div id="cover-errors" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="descriptionEn">Blog Description En</label>
                        <textarea  class="form-control" name="descriptionEn" id="descriptionEn" placeholder="Enter Description En" required></textarea>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="descriptionAr">Blog Description Ar</label>
                        <textarea  class="form-control" name="descriptionAr" id="descriptionAr" placeholder="Enter Description Ar" required></textarea>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaImg">SEO Meta Image</label>
                        <div class="custom-file">
                            <input type="file"  id="metaImg" name="metaImg" data-parsley-errors-container="#meta-errors"  required>

                        </div>
                        <div id="meta-errors" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaTitle">SEO Meta Title</label>
                        <input type="text" class="form-control" name="metaTitle" id="metaTitle" placeholder="Enter Meta Title" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaKeywords">SEO Meta Keywords</label>
                        <input type="text" class="form-control" name="metaKeywords" id="metaKeywords" placeholder="Enter Meta Keywords" data-role="tagsinput" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaDescription">SEO Meta Description</label>
                        <textarea type="text" class="form-control" name="metaDescription" id="metaDescription" placeholder="Meta Description..."></textarea>
                    </div>

                </form>
                <button  class="btn btn-success submit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/blogs.js')}}"></script>

