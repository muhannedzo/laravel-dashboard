<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit Social Media</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="editForm" data-parsley-validate>
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group">
                        <label class="text-white" for="icon">Font Awesome Icon</label>
                        <input type="text" class="form-control" name="icon" id="icon" placeholder="Font Awesome Icon" value="{{$data['icon']}}" >
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="url">URL</label>
                        <input type="url" class="form-control" name="url" id="url" placeholder="URL" value="{{$data['url']}}" >
                    </div>
                </form>
                <button type="button" class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/socials.js')}}"></script>

