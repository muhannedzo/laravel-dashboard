<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Create FAQ</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="createForm" data-parsley-validate>
                    <div class="faq">
                        <div class="form-group">
                            <label class="text-white" for="q1En">Question En</label>
                            <input type="text" class="form-control" name="q1En" id="q1En" placeholder="Question En" required >
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="q1Ar">Question Ar</label>
                            <input type="text" class="form-control" name="q1Ar" id="q1Ar" placeholder="Question Ar"  required>
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="a1En">Answer En</label>
                            <input type="text" class="form-control" name="a1En" id="a1En" placeholder="Answer En"  required>
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="a1Ar">Answer Ar</label>
                            <input type="text" class="form-control" name="a1Ar" id="a1Ar" placeholder="Answer Ar"  required>
                        </div>
                    </div>

                </form>
                <div>
                <button class="btn btn-primary plus mt-3 mb-5"><i class="fa fa-plus"></i></button>
                </div>
                <button  class="btn btn-success submit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/faqs.js')}}"></script>

