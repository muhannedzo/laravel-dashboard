<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit FAQ</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="editForm" data-parsley-validate>
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group">
                        <label class="text-white" for="qEn">Question En</label>
                        <input type="text" class="form-control" name="qEn" id="qEn" placeholder="Question En" value="{{$data['question_en']}}" >
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="qAr">Question Ar</label>
                        <input type="text" class="form-control" name="qAr" id="qAr" placeholder="Question Ar" value="{{$data['question_ar']}}" >
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="aEn">Answer En</label>
                        <input type="text" class="form-control" name="aEn" id="aEn" placeholder="Answer En" value="{{$data['answer_en']}}" >
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="aAr">Answer Ar</label>
                        <input type="text" class="form-control" name="aAr" id="aAr" placeholder="Answer Ar" value="{{$data['answer_ar']}}" >
                    </div>




                </form>
                <button type="button" class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/faqs.js')}}"></script>

