<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">All Bookings</h1>
            </div>
        </div>
    </div>
</section>
<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <table class="table table-striped table-bordered data-table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Invoice Id</th>
                        <th>Rental Number</th>
                        <th>Rental Type</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Customer Name</th>
                        <th>Customer Phone</th>
                        <th>Payment</th>
                        <th>Baby Seat</th>
                        <th>Driver</th>
                        <th>Total</th>
                        <th>State</th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<x-admin.footer/>
<script src="{{asset('js/admin/bookings.js')}}"></script>
