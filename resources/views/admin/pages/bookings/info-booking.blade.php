<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Booking Info</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12 col-md-6 my-2">
                <span class="text-white fs-20">Invoice Id: {{$data[0]['invoice_id']}}</span>
            </div><div class="col-12 col-md-6 my-2">
                <span class="text-white fs-20">Rental Number: {{$data[0]['rental_number']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20">Rental Type: {{$data[0]['rental_type']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white   fs-20">Car Model: {{$data[0]['cars']['model']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white      fs-20">Car Year: {{$data[0]['cars']['year']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white {{$data[0]['rental_type']=='hourly'?'badge badge-success':''}} fs-20">Car Hourly Price: {{$data[0]['cars']['hourly_price']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white {{$data[0]['rental_type']=='daily'?'badge badge-success':''}} fs-20">Car Daily Price: {{$data[0]['cars']['daily_price']}}</span>
            </div>

            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20">Car Outer Color: <div class="d-inline-block mx-2" style="height: 15px;width: 15px;background-color: {{$data[0]['cars']['outer_color']}}"></div></span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20">Car Inner Color: <div class="d-inline-block mx-2" style="height: 15px;width: 15px;background-color: {{$data[0]['cars']['inner_color']}}"></div></span>
            </div>

            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20">From Date: {{$data[0]['from_date']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20">To Date: {{$data[0]['to_date']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20">Customer Name: {{$data[0]['customer_name']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20">Customer Phone: <a class="text-white  fs-20" href="tel:{{$data[0]['customer_phone']}}">{{$data[0]['customer_phone']}}</a></span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20">Customer Email: <a class="text-white  fs-20" href="mailto:{{$data[0]['customer_email']}}">{{$data[0]['customer_email']}}</a></span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20">Payment: {{$data[0]['payment_method']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class=" {{$data[0]['has_babyseat']==1?'badge badge-success':'badge badge-danger'}}  fs-20">Baby Seat: {{$data[0]['has_babyseat']==1?'Include':'Not Include'}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class=" {{$data[0]['has_driver']==1?'badge badge-success':'badge badge-danger'}} fs-20">Driver: {{$data[0]['has_driver']==1?'Include':'Not Include'}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class=" {{$data[0]['state']==1?'badge badge-info':'badge badge-danger'}} fs-20">State: {{$data[0]['has_driver']==1?'In Process':'Pending'}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white badge badge-secondary  fs-20">Total: {{$data[0]['total_price']}}</span>
            </div>
            <div class="col-12 col-md-6 my-2">
                <span class="text-white  fs-20"></span>
            </div>
        </div>
    </div>
</section>
<x-admin.footer/>



