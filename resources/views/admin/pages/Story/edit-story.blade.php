<x-admin.header/>
<x-admin.nav page="{{$page}}"/>

<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit Story</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 row" id="editForm" data-parsley-validate>
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="title">Title</label>
                        <input type="text" class="form-control" name="title" id="title" value="{{$data['title']}}" placeholder="Enter Title"
                               required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="slug">Slug</label>
                        <input type="text" class="form-control" name="slug" id="slug" value="{{$data['slug']}}" placeholder="Enter Slug" required>
                    </div>
                    <div class="form-group col-12">
                        <label class="text-white" for="description">Description En</label>
                        <textarea type="text" class="form-control" name="description" id="description"
                                  placeholder="Description ...">{{$data['description']}}</textarea>
                    </div>
                    <div class="form-group col-12">
                        <label class="text-white" for="img">Image</label>
                        <div class="custom-file">
                            <input type="file" id="img" name="img" data-parsley-errors-container="#meta-errors" data-value="{{$data['image']}}"
                                   >
                        </div>
                        <div id="meta-errors" class="mt-1"></div>
                    </div>
                </form>
                <button class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">
                    Save
                </button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/story.js')}}"></script>

