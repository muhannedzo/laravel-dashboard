<x-admin.header/>
<x-admin.nav page="{{$page}}"/>

<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Create Story</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 row" id="createForm" data-parsley-validate>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="title">Story Title</label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Enter Title"
                               required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="slug">Slug</label>
                        <input type="text" class="form-control" name="slug" id="slug" placeholder="Enter Slug" required>
                    </div>
                    <div class="form-group col-12">
                        <label class="text-white" for="description">Description En</label>
                        <textarea type="text" class="form-control" name="description" id="description"
                                  placeholder="Description En..."></textarea>
                    </div>
                    <div class="form-group col-12">
                        <label class="text-white" for="img">Image</label>
                        <div class="custom-file">
                            <input type="file" id="img" name="img" data-parsley-errors-container="#meta-errors"
                                   required>
                        </div>
                        <div id="meta-errors" class="mt-1"></div>
                    </div>

                </form>
                <button class="btn btn-success submit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">
                    Save
                </button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/story.js')}}"></script>

