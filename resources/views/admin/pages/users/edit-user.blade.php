<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit User</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container mt-5 text-white">
        <div class="row">
            <div class="col-12">
                <div class="d-flex">
                    <label class="text-white" >Old Image</label>
                    <img src="{{asset($data['avatar'])}}" class="w-20 rounded-circle m-auto">
                </div>
                <form class="w-100 " id="updateForm" data-parsley-validate >
                    <input type="hidden" id="id" value="{{$data['id']}}">
                    <div class="form-group">
                        <label class="text-white" for="avatar">{{__('user.ProfileImage')}}</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="avatar" name="filename">
                            <label class="custom-file-label" for="avatar">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{$data['name']}}" placeholder="Enter name" required >
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" name="email" id="email" value="{{$data['email']}}" placeholder="Enter email" required >
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="role">Role</label>
                        <select id="role" class="form-control" required>
                            <option disabled selected>Select Role...</option>
                            @foreach($roles as $role)
                                <option value="{{$role->name}}" <?php echo $userRole==$role->name?'selected':'';?>>{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">Edit</button>
                    <div class="msg"></div>
                </form>
            </div>
        </div>
    </div>
</section>
<x-admin.footer/>
<script src="{{asset('js/admin/users.js')}}"></script>

