<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">All Users</h1>
                <div><a class="btn btn-success" href="{{route('create-user')}}"><i class="fa fa-user-plus"></i> Create User</a></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card my-3 table-filter" >
                    <div class="card-body">
                        <div class="form-group">
                            <label><strong>State :</strong></label>
                            <select id='stateFilter' class="form-control" style="width: 200px">
                                <option value="">All</option>
                                <option value="1">Active</option>
                                <option value="2">Unactivated</option>
                            </select>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered data-table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Avatar</th>
                        <th>Email</th>
                        <th>State</th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<x-admin.footer/>
<script src="{{asset('js/admin/users.js')}}"></script>
