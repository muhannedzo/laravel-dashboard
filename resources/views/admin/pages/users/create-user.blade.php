<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Create User</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="createUserForm" data-parsley-validate>
                    {{--                @method('PUT')--}}
                    <div class="form-group">
                        <label class="text-white" for="avatar">{{__('user.ProfileImage')}}</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="avatar" name="filename">
                            <label class="custom-file-label" for="avatar">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="name">{{__('user.Name')}}</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter name" required>
                        {{--                    @error('email'){{$message}}@enderror--}}
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="email">{{__('user.EmailAddress')}}</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Enter email"
                               required>
                        {{--                    @error('email'){{$message}}@enderror--}}
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="role">Role</label>
                        <select id="role" class="form-control" required>
                            <option disabled selected>Select Role...</option>
                            @foreach($roles as $role)
                                <option value="{{$role->name}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="password">{{__('user.Password')}}</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password"
                               required>
                    </div>
                    <button type="submit" class="btn btn-success">Submit</button>
                    <div class="msg"></div>
                    {{--                <div>--}}
                    {{--                    @if($errors->any())--}}
                    {{--                        @foreach($errors->all() as $error)--}}
                    {{--                        {{$error}}--}}
                    {{--                        @endforeach--}}
                    {{--                    @endif--}}
                    {{--                </div>--}}
                </form>
            </div>
        </div>
    </div>
</section>
<x-admin.footer/>
<script src="{{asset('js/admin/users.js')}}"></script>
