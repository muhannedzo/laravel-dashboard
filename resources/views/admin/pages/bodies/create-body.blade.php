<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Create Body</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="createForm" data-parsley-validate>
                    <div class="form-group">
                        <label class="text-white" for="nameEn">Body Name En</label>
                        <input type="text" class="form-control" name="nameEn" id="nameEn" placeholder="Enter Name En" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="nameAr">Body Name Ar</label>
                        <input type="text" class="form-control" name="nameAr" id="nameAr" placeholder="Enter Name Ar" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="bodyImg">Body Image</label>
                        <div class="custom-file">
                            <input id="bodyImg" name="bodyImg" type="file" data-parsley-errors-container="#bodyImg-errors" required>
                        </div>
                        <div id="bodyImg-errors" class="mt-1"></div>
                    </div>

                </form>
                <button  class="btn btn-success submit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/bodies.js')}}"></script>

