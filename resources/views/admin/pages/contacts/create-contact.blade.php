<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Create Contact</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="createForm" data-parsley-validate>
                    <div class="contact">
                        <div class="form-group">
                            <label class="text-white" for="type1">Type</label>
                            <select  class="form-control" name="type1" id="type1" required>
                                <option disabled="disabled" selected>Type...</option>
                                <option value="phone">phone</option>
                                <option value="mobile">mobile</option>
                                <option value="mail">mail</option>
                                <option value="whatsapp">whatsapp</option>
                                <option value="url">url</option>
                                <option value="address">address</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="text-white" for="title1">Title</label>
                            <input type="text" class="form-control" name="title1" id="title1" placeholder="Enter Title"  required>
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="icon1">Font Awesome Icon</label>
                            <input type="text" class="form-control" name="icon1" id="icon1" placeholder="Font Awesome Icon" required >
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="url1">URL</label>
                            <input type="text" class="form-control" name="url1" id="url1" placeholder="URL" required>
                        </div>
                    </div>

                </form>
                <div>
                <button class="btn btn-primary plus mt-3 mb-5"><i class="fa fa-plus"></i></button>
                </div>
                <button  class="btn btn-success submit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/contacts.js')}}"></script>

