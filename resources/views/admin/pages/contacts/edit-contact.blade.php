<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit Social Media</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="editForm" data-parsley-validate>
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group">
                        <label class="text-white" for="type">Title En</label>
                        <select  class="form-control" name="type" id="type" required>
                            <option value="phone" <?php echo $data['type']=='phone'?'selected':'' ?>>phone</option>
                            <option value="mobile" <?php echo $data['type']=='mobile'?'selected':'' ?>>mobile</option>
                            <option value="mail" <?php echo $data['type']=='mail'?'selected':'' ?>>mail</option>
                            <option value="whatsapp" <?php echo $data['type']=='whatsapp'?'selected':'' ?>>whatsapp</option>
                            <option value="url" <?php echo $data['type']=='url'?'selected':'' ?>>url</option>
                            <option value="address" <?php echo $data['type']=='address'?'selected':'' ?>>address</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="title">Title </label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Enter Title " value="{{$data['title']}}"  required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="icon">Font Awesome Icon</label>
                        <input type="text" class="form-control" name="icon" id="icon" placeholder="Font Awesome Icon" value="{{$data['icon']}}" required >
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="url">URL</label>
                        <input type="url" class="form-control" name="url" id="url" placeholder="URL" value="{{$data['url']}}" required>
                    </div>
                </form>
                <button type="button" class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/contacts.js')}}"></script>

