<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Create Role</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container mt-5 text-white">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="createRoleForm" data-parsley-validate>
                    <div class="form-group">
                        <label class="text-white" for="role">Role Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter role" required>
                    </div>
                    <h2 class="my-4">Permissions</h2>
                    @foreach($permissions as $permission)
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input permission" type="checkbox" name="permission"  value="{{$permission->name}}" >
                            <label class="form-check-label" >
                               {{$permission->name}}
                            </label>
                        </div>

                    </div>
                    @endforeach
                    <button type="submit" class="btn btn-success">Submit</button>

                </form>
            </div>
        </div>
    </div>
</section>
<x-admin.footer/>
<script src="{{asset('js/admin/roles.js')}}"></script>
