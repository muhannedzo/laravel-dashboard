<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">About Us</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="aboutForm" data-parsley-validate>
                    @if(isset($data['id']))
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group">
                        <label class="text-white" for="titleEn">About Title En</label>
                        <textarea  class="form-control textArea" name="titleEn" id="titleEn" placeholder="Enter title En"  required>{{$data['title_en']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="titleAr">About Title Ar</label>
                        <textarea  class="form-control textArea" name="titleAr" id="titleAr" placeholder="Enter title Ar"  required>{{$data['title_ar']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="cover">About Cover</label>
                        <div class="custom-file">
                            <input id="cover" name="cover" type="file" data-parsley-errors-container="#cover-errors"  data-value="{{$data['cover']}}" >
                        </div>
                        <div id="cover-errors" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="descriptionEn">About Description En</label>
                        <textarea  class="form-control textArea" name="description" id="descriptionEn" placeholder="Enter Description En"  required>{{$data['description_en']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="descriptionAr">About Description Ar</label>
                        <textarea  class="form-control textArea" name="description" id="descriptionAr" placeholder="Enter Description Ar"  required>{{$data['description_ar']}}</textarea>
                    </div>
                        @else
                        <div class="form-group">
                            <label class="text-white" for="titleEn">About Title En</label>
                            <textarea  class="form-control textArea" name="titleEn" id="titleEn" placeholder="Enter title En"  required></textarea>
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="titleAr">About Title Ar</label>
                            <textarea  class="form-control textArea" name="titleAr" id="titleAr" placeholder="Enter title Ar" required></textarea>
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="cover">About Cover</label>
                            <div class="custom-file">
                                <input id="cover" name="cover" type="file" data-parsley-errors-container="#cover-errors"   >
                            </div>
                            <div id="cover-errors" class="mt-1"></div>
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="descriptionEn">About Description En</label>
                            <textarea  class="form-control textArea" name="description" id="descriptionEn" placeholder="Enter Description En"  required></textarea>
                        </div>
                        <div class="form-group">
                            <label class="text-white" for="descriptionAr">About Description Ar</label>
                            <textarea  class="form-control textArea" name="description" id="descriptionAr" placeholder="Enter Description Ar"  required></textarea>
                        </div>
                    @endif

                </form>
                <button type="button" class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/about.js')}}"></script>

