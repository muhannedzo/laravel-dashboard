<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit Brand</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 " id="editBrandForm" data-parsley-validate>
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group">
                        <label class="text-white" for="name">Brand Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="{{$data['name']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="slug">Brand Slug</label>
                        <input type="text" class="form-control" name="slug" id="slug" placeholder="Enter Slug" value="{{$data['slug']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="brandImg">Brand Image</label>
                        <div class="custom-file">
                            <input id="brandImg" name="brandImg" type="file" data-parsley-errors-container="#brandImg-errors"  data-value="{{$data['img']}}" >
                        </div>
                        <div id="brandImg-errors" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="cover">Brand Cover</label>
                        <div class="custom-file">
                            <input type="file" id="cover" name="cover" data-parsley-errors-container="#cover-errors"  data-value="{{$data['cover']}}">
                        </div>
                        <div id="cover-errors" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="order">Brand Order</label>
                        <input type="number" class="form-control" name="order" id="order" placeholder="Order Number" value="{{$data['order_num']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="description">Brand Description</label>
                        <textarea type="text" class="form-control" name="description" id="description" placeholder="Description...">{{$data['description']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaImg">SEO Brand Meta Image</label>
                        <div class="custom-file">
                            <input type="file"  id="metaImg" name="metaImg" data-parsley-errors-container="#meta-errors"  data-value="{{$data['meta_image']}}">

                        </div>
                        <div id="meta-errors" class="mt-1"></div>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaTitle">SEO Brand Meta Title</label>
                        <input type="text" class="form-control" name="metaTitle" id="metaTitle" placeholder="Enter Meta Title" value="{{$data['meta_title']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaKeywords">SEO Brand Meta Keywords</label>
                        <input type="text" class="form-control" name="metaKeywords" id="metaKeywords" placeholder="Enter Meta Keywords" value="{{$data['meta_keywords']}}" data-role="tagsinput" required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="metaDescription">SEO Brand Meta Description</label>
                        <textarea type="text" class="form-control" name="metaDescription" id="metaDescription" placeholder="Meta Description...">{{$data['meta_description']}}</textarea>
                    </div>

                </form>
                <button type="button" class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">Save</button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/brands.js')}}"></script>

