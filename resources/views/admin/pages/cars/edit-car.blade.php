<x-admin.header/>
<x-admin.nav page="{{$page}}"/>
<?php
$brands = \App\Models\Brand::all();
$types = \App\Models\Type::all();
$bodies = \App\Models\Body::all();
?>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h1 class="text-white d-inline-block font-weight-bold">Edit Car</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <form class="w-100 row" id="editForm" data-parsley-validate>
                    <input type="hidden" value="{{$data['id']}}" id="id">
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="type">Type</label>
                        <select class="form-control" name="type" id="type" data-parsley-errors-container="#type-errors" required>
                            <option disabled="disabled" selected>Type..</option>
                            @foreach($types as $type)
                                <option value="{{$type['id']}}" <?php echo $data['type_id']==$type['id']?'selected':''; ?>>{{$type['name_en']}}</option>
                            @endforeach
                        </select>
                        <div id="type-errors" class="mt-1"></div>
                    </div>

                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="brand">Brand</label>
                        <select class="form-control" name="brand" id="brand" data-parsley-errors-container="#brand-errors" required>
                            <option disabled="disabled" selected>Brand..</option>
                            @foreach($brands as $brand)
                                <option value="{{$brand['id']}}" <?php echo $data['brand_id']==$brand['id']?'selected':''; ?> >{{$brand['name']}}</option>
                            @endforeach
                        </select>
                        <div id="brand-errors" class="mt-1"></div>
                    </div>

                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="body">Body</label>
                        <select class="form-control" name="body" id="body" data-parsley-errors-container="#body-errors" required>
                            <option disabled="disabled" selected>Body..</option>
                            @foreach($bodies as $body)
                                <option value="{{$body['id']}}" <?php echo $data['body_id']==$body['id']?'selected':''; ?>>{{$body['name_en']}}</option>
                            @endforeach
                        </select>
                        <div id="body-errors" class="mt-1"></div>
                    </div>

                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="model">Model Name</label>
                        <input type="text" class="form-control" name="model" id="model" value="{{$data['model']}}" placeholder="Enter Model"
                               required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="daily">Daily Price</label>
                        <input type="number"  class="form-control" name="daily" id="daily" value="{{$data['daily_price']}}"
                               placeholder="Enter Daily Price" required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="hourly">Hourly</label>
                        <input type="number"  class="form-control" name="hourly" id="hourly" value="{{$data['hourly_price']}}"
                               placeholder="Enter Hourly Price" required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="year">Year</label>
                        <input type="number" min="2015" class="form-control" name="year" id="year" value="{{$data['year']}}"
                               placeholder="Enter Year" required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="seats">Seats</label>
                        <input type="number"  class="form-control" name="seats" id="seats" value="{{$data['seats']}}"
                               placeholder="Enter Number Of Seats" required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="outer">Outer Color</label>
                        <input type="color"  class="form-control" name="outer" id="outer" value="{{$data['outer_color']}}" required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="inner">Inner Color</label>
                        <input type="color"  class="form-control" name="inner" id="inner" value="{{$data['inner_color']}}" required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="slug">Slug</label>
                        <input type="text" class="form-control" name="slug" id="slug" value="{{$data['slug']}}" placeholder="Enter Slug" required>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label class="text-white" for="slugGroup">Slug Group</label>
                        <input type="text" class="form-control" name="slugGroup" id="slugGroup" value="{{$data['slug_group']}}"
                               placeholder="Enter Slug Group" required>
                    </div>
                    <div class="form-group col-12 ">
                        <label class="text-white" for="imgs">Images</label>
                        <div class="custom-file">
                            <input id="imgs" name="imgs" type="file" data-parsley-errors-container="#imgs-errors" data-value="{{$data['imgs']}}"
                                   multiple >
                        </div>
                        <div id="imgs-errors" class="mt-1"></div>
                    </div>


                    <div class="form-group col-12">
                        <label class="text-white" for="descriptionEn">Description En</label>
                        <textarea type="text" class="form-control" name="descriptionEn" id="descriptionEn"
                                  placeholder="Description En...">{{$data['description_en']}}</textarea>
                    </div>
                    <div class="form-group col-12">
                        <label class="text-white" for="descriptionAr">Description Ar</label>
                        <textarea type="text" class="form-control" name="descriptionAr" id="descriptionAr"
                                  placeholder="Description Ar...">{{$data['description_ar']}}</textarea>
                    </div>
                    <div class="form-group col-12">
                        <label class="text-white" for="metaImg">SEO Meta Image</label>
                        <div class="custom-file">
                            <input type="file" id="metaImg" name="metaImg" data-parsley-errors-container="#meta-errors" data-value="{{$data['meta_image']}}"
                                   >

                        </div>
                        <div id="meta-errors" class="mt-1"></div>
                    </div>
                    <div class="form-group col-12 ">
                        <label class="text-white" for="metaTitle">SEO Meta Title</label>
                        <input type="text" class="form-control" name="metaTitle" id="metaTitle" value="{{$data['meta_title']}}"
                               placeholder="Enter Meta Title" required>
                    </div>
                    <div class="form-group col-12 ">
                        <label class="text-white" for="metaKeywords">SEO Meta Keywords</label>
                        <input type="text" class="form-control" name="metaKeywords" id="metaKeywords" value="{{$data['meta_keywords']}}"
                               placeholder="Enter Meta Keywords" data-role="tagsinput" required>
                    </div>
                    <div class="form-group col-12">
                        <label class="text-white" for="metaDescription">SEO Meta Description</label>
                        <textarea type="text" class="form-control" name="metaDescription" id="metaDescription"
                                  placeholder="Meta Description...">{{$data['meta_description']}}</textarea>
                    </div>

                </form>
                <button class="btn btn-success submitEdit" data-loading="<i class='fa fa-spinner fa-spin '></i> Loading...">
                    Save
                </button>

            </div>
        </div>
    </div>
</section>
<x-admin.footer/>

<script src="{{asset('js/admin/cars.js')}}"></script>

