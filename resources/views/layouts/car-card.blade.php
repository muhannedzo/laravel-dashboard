<div class="col-12 col-sm-6 col-md-6 col-xl-4  my-2 {{$car->bodies->name_en}} ALL car-selector type-{{$car->types->id}}" >
    <img class="card-brand" src="<?php echo url('/').'/';?>{{$car->brands->img}}">
    <div class="car-card text-center">
        <a   href="{{route('details',['group'=>$car->slug_group,"car"=>$car->slug])}}">
        <div class="car-slider">
            <?php $images = explode(',',$car->imgs);?>
            @foreach($images as $img)
                <div>
                    <img data-lazy="<?php echo url('/').'/';?>{{$img}}" class="w-100 ">
                </div>
            @endforeach
        </div>
        </a>
        <a   href="{{route('details',['group'=>$car->slug_group,"car"=>$car->slug])}}">
        <h4 class="gold pt-1">{{$car->model}}</h4>
        </a>
        <div class="car-card-details text-center">

            <div class="details-2">
                <div class="row px-3 pt-1">
                    <div class="col-12 text-white d-flex justify-content-center mb-3">
                        <span class="">{{$car->hourly_price}} AED <em class="gray3"
                                                                      style="font-weight:100; ">Hourly</em> </span>
                        <div class="mx-3" style="width: 2px; background-color: var(--gold)"></div>
                        <span class="">{{$car->daily_price}} AED <em class="gray3"
                                                                     style="font-weight:100; ">Day</em> </span>
                    </div>
                </div>
                <div class="container-fluid px-0 text-white">
                    <div class="row" style="direction: ltr">
                        <div class="col-3 pr-1">
                            <a class="btn btn-outline-dark d-inline-block  w-100 py-2 btn-first"
                               href="https://api.whatsapp.com/send?phone=" target="_blank"><i
                                        class="fa fa-whatsapp"></i> Whatsapp
                            </a>
                        </div>
                        <div class="col-3 px-1">
                            <a class="btn btn-outline-dark d-inline-block  w-100 py-2 btn-2"
                               href="tel:+"><i
                                        class="fa fa-phone"></i> Call
                            </a>
                        </div>
                        <div class="col-3 px-1">
                            <a class="btn btn-outline-dark d-inline-block  w-100 py-2 btn-3"
                             href="{{route('details',['group'=>$car->slug_group,"car"=>$car->slug])}}"><i
                                        class="fa fa-info-circle"></i> Details</a>
                        </div>
                        <div class="col-3 pl-1"><a
                                    class="btn btn-outline-dark d-inline-block  w-100 py-2 btn-last bookNow"
                                    data-carid="{{$car->id}}"
                                    data-name="{{$car->brands->name}} {{$car->model}}"
                                    data-daily="{{$car->daily_price}}"
                                    data-hourly="{{$car->hourly_price}}"
                                    data-fee=""><i
                                        class="fa fa-car"></i> Book
                            </a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
