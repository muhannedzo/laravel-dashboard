<div class="sidebar">
    <div class="logo_content d-flex">
        <div class="logo m-auto">
            <div class="logo_name"><img src="{{asset('img/logo.svg')}}" class="w-75"></div>
        </div>
        <i class='fa fa-bars' id="btn"></i>
    </div>
    <ul class="nav_list">
        @if(auth()->user()->can('cars manage'))
            <li class="<?php echo isset($page) && $page == 'allTypes' ? 'activeBar' : ''; ?>">
                <a href="{{ route('allTypes') }}">
                    <i class='fa fa-rocket'></i>
                    <span class="links_name">Types</span>
                </a>
                <span class="tooltip">Types</span>

            </li>
            <li class="<?php echo isset($page) && $page == 'allBrands' ? 'activeBar' : ''; ?>">
                <a href="{{ route('allBrands') }}">
                    <i class='fa fa-connectdevelop'></i>
                    <span class="links_name">Brands</span>
                </a>
                <span class="tooltip">Brands</span>

            </li>
            <li class="<?php echo isset($page) && $page == 'allBodies' ? 'activeBar' : ''; ?>">
                <a href="{{ route('allBodies') }}">
                    <i class='fa fa-truck'></i>
                    <span class="links_name">Bodies</span>
                </a>
                <span class="tooltip">Bodies</span>

            </li>
            <li class="<?php echo isset($page) && $page == 'allCars' ? 'activeBar' : ''; ?>">
                <a href="{{ route('allCars') }}">
                    <i class='fa fa-car'></i>
                    <span class="links_name">Cars</span>
                </a>
                <span class="tooltip">Cars</span>

            </li>
        @endif
        @if(auth()->user()->can('bookings manage'))
            <li class="<?php echo isset($page) && $page == 'allBookings' ? 'activeBar' : ''; ?>">
                <a href="{{ route('allBookings') }}">
                    <i class='fa fa-credit-card'></i>
                    <span class="links_name">Bookings</span>
                </a>
                <span class="tooltip">Bookings</span>

            </li>
        @endif
            @if(auth()->user()->can('blogs manage'))
        <li class="<?php echo isset($page) && $page == 'allBlogs' ? 'activeBar' : ''; ?>">
            <a href="{{ route('allBlogs') }}">
                <i class='fa fa-pencil-square-o'></i>
                <span class="links_name">Blog</span>
            </a>
            <span class="tooltip">Blog</span>

        </li>
            @endif
            @if(auth()->user()->can('content manage'))
        <li class="<?php echo isset($page) && $page == 'allFaqs' ? 'activeBar' : ''; ?>">
            <a href="{{ route('allFaqs') }}">
                <i class='fa fa-question-circle-o'></i>
                <span class="links_name">FAQ</span>
            </a>
            <span class="tooltip">FAQ</span>

        </li>

        <li class="<?php echo isset($page) && $page == 'about' ? 'activeBar' : ''; ?>">
            <a href="{{ route('getAbout') }}">
                <i class='fa fa-info-circle'></i>
                <span class="links_name">About</span>
            </a>
            <span class="tooltip">About</span>

        </li>


        <li class="<?php echo isset($page) && $page == 'allTerms' ? 'activeBar' : ''; ?>">
            <a href="{{ route('allTerms') }}">
                <i class='fa fa-check'></i>
                <span class="links_name">Terms</span>
            </a>
            <span class="tooltip">Terms</span>

        </li>
        <li class="<?php echo isset($page) && $page == 'allSocials' ? 'activeBar' : ''; ?>">
            <a href="{{ route('allSocials') }}">
                <i class='fa fa-facebook'></i>
                <span class="links_name">Socials Media</span>
            </a>
            <span class="tooltip">Socials Media</span>

        </li>
        <li class="<?php echo isset($page) && $page == 'allContacts' ? 'activeBar' : ''; ?>">
            <a href="{{ route('allContacts') }}">
                <i class='fa fa-phone'></i>
                <span class="links_name">Contacts</span>
            </a>
            <span class="tooltip">Contacts</span>

        </li>
            @endif
        @if(auth()->user()->can('view user'))
            <li class="<?php echo isset($page) && $page == 'allUsers' ? 'activeBar' : ''; ?>">
                <a href="{{ route('allUsers') }}">
                    <i class='fa fa-user'></i>
                    <span class="links_name">All Users</span>
                </a>
                <span class="tooltip">All Users</span>

            </li>
        @endif
        @if(auth()->user()->can('add user'))
            <li class="<?php echo isset($page) && $page == 'create-user' ? 'activeBar' : ''; ?>">
                <a href="{{ route('create-user') }}">
                    <i class='fa fa-user-plus'></i>
                    <span class="links_name">Create User</span>
                </a>
                <span class="tooltip">Create User</span>

            </li>
        @endif
        @if(auth()->user()->can('view role'))
            <li class="<?php echo isset($page) && $page == 'all-roles' ? 'activeBar' : ''; ?>">
                <a href="{{ route('all-roles') }}">
                    <i class='fa fa-university'></i>
                    <span class="links_name">All Roles</span>
                </a>
                <span class="tooltip">All Roles</span>

            </li>
        @endif
        @if(auth()->user()->can('add role'))
            <li class="<?php echo isset($page) && $page == 'create-role' ? 'activeBar' : ''; ?>">
                <a href="{{ route('create-role') }}">
                    <i class='fa fa-gavel'></i>
                    <span class="links_name">Create Role</span>
                </a>
                <span class="tooltip">Create Role</span>

            </li>
        @endif
        @if(auth()->user()->can('manage stories'))
            <li class="<?php echo isset($page) && $page == 'all-stories' ? 'activeBar' : ''; ?>">
                <a href="{{ route('all-stories') }}">
                    <i class='fa fa-gavel'></i>
                    <span class="links_name">All Stories</span>
                </a>
                <span class="tooltip">All Stories</span>

            </li>
        @endif
    </ul>
    <div class="content">
        <div class="user">
            <div class="user_details">
                <img src="{{asset(auth()->user()->avatar)}}"
                     alt="{{auth()->user()->name}}"/>
                <div class="name_job">
                    <div class="name">{{auth()->user()->name}}</div>
                    <div class="job">{{auth()->user()->roles[0]->name}}</div>
                </div>
            </div>
            <i class='fa fa-power-off logout' id="log_out" title="Logout"></i>

        </div>
    </div>
</div>







