</body>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('js/moment.js')}}"></script>
<script src="{{asset('js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('js/parsley.min.js')}}"></script>
<script src="{{asset('js/slick.min.js')}}"></script>
<script src="{{asset('js/intlTelInput-jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{asset('js/dataTable.min.js')}}"></script>
<script src="{{asset('js/dataTableBootstrap.min.js')}}"></script>
<script src="{{asset('js/wow.js')}}"></script>
<script src="{{asset('js/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/piexif.min.js')}}"></script>
<script src="{{asset('js/sortable.min.js')}}"></script>
<script src="{{asset('js/fileinput.min.js')}}"></script>
<script src="{{asset('js/trumbowyg.min.js')}}"></script>
<script src="{{asset('js/select2.full.min.js')}}"></script>

<script type="text/javascript">
    var base_url =@json(url('/'))+'/';
</script>
<script src="{{asset('js/admin/main.js')}}"></script>


</html>
