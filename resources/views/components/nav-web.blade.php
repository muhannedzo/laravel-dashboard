<?php
$social_item = \App\Models\Social::get();
$brands = \App\Models\Brand::get();

?>
<nav class="navbar navbar-expand-lg navbar-light px-4 px-md-5 fixed-top d-none d-md-flex">
    <a class="navbar-brand" href="{{route('home')}}"><img src="{{asset('img/logo.svg')}}" class=""></a>
    <div>
        <form class="form-inline my-2 my-lg-0 d-lg-none d-inline searchForm">
            <div class="search-input " style="display:none">
                <input class="form-control mr-sm-2 search-car-input" type="search" placeholder="Search" aria-label="Search">
                <div class="search-result">


                </div>
            </div>
            <a class="my-2 my-sm-0 search-icon" type="submit"><i class="fa fa-search fs-24 text-white"></i></a>
        </form>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars text-white"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
        <ul class="navbar-nav m-auto">
            <li class="nav-item <?php echo $page == 'home' ? 'active' : '';?>">
                <a class="nav-link" href="{{route('home')}}">HOME</a>
            </li>
            {{--            <li class="nav-item <?php echo $page=='fleet'?'active':'';?> dropdown">--}}
            {{--                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
            {{--                    OUR FLEET--}}
            {{--                </a>--}}
            {{--                <div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
            {{--                    <a class="dropdown-item" href="#">Luxury Car</a>--}}
            {{--                    <a class="dropdown-item" href="#">Sport Car</a>--}}
            {{--                    <a class="dropdown-item" href="#">SUV Car</a>--}}
            {{--                    <a class="dropdown-item" href="#">Economy Car</a>--}}
            {{--                    <a class="dropdown-item" href="#">Convertible Car</a>--}}
            {{--                </div>--}}
            {{--            </li>--}}
            <li class="nav-item <?php echo $page == 'brand' ? 'active' : '';?> dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarBrandDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    BRANDS
                </a>
                <div class="dropdown-menu brand-menu" aria-labelledby="navbarBrandDropdown">
                    @foreach($brands as $brand)
                        <div class="d-inline-block">
                            <a href="{{route('brand',[$brand->slug])}}">
                                <img src="{{url('/').'/'}}{{$brand->img}}" class="mxw-150">
                            </a>
                        </div>

                    @endforeach
                </div>
            </li>
            <li class="nav-item <?php echo $page == 'about' ? 'active' : '';?>">
                <a class="nav-link" href="{{route('about')}}">ABOUT US</a>
            </li>
            <li class="nav-item <?php echo $page == 'car-list' ? 'active' : '';?>">
                <a class="nav-link" href="{{route('car-list')}}">CARS LIST</a>
            </li>
            </li>
            <li class="nav-item <?php echo $page == 'terms' ? 'active' : '';?>">
                <a class="nav-link" href="{{route('terms')}}">RENT TERMS</a>
            </li>
            <li class="nav-item <?php echo $page == 'faq' ? 'active' : '';?>">
                <a class="nav-link" href="{{route('faq')}}">FAQ</a>
            </li>
            <li class="nav-item <?php echo $page == 'blog' ? 'active' : '';?>">
                <a class="nav-link" href="{{route('blog')}}">BLOG</a>
            </li>


            <li class="nav-item <?php echo $page == 'contact' ? 'active' : '';?>">
                <a class="nav-link" href="{{route('contact')}}">CONTACT US</a>
            </li>
        </ul>
        <div class="d-block d-lg-none">
            <p class="mt-5">
                @foreach($social_item as $item)
                    <a href="{{$item->url}}" target="_blank" class="text-white mr-5 fs-30"><i
                            class="text-white fa fa-{{$item->icon}}"></i></a>
                @endforeach

            </p>
        </div>
        <form class="form-inline my-2 my-lg-0 justify-content-center d-none d-lg-block searchForm">
            <div class="search-input" style="display:none">
                <input class="form-control mr-sm-2 search-car-input" type="search" placeholder="Search" aria-label="Search">
                <div class="search-result">


                </div>
            </div>
            <a class="my-2 my-sm-0 search-icon" type="submit"><i class="fa fa-search fs-24 text-white"></i></a>
        </form>
    </div>

</nav>
<div class="nav d-block d-md-none">
    <div class="nav-div">
        <div class="text-center w-100 scroll-menu">
            <div class="mt-5">
                <a href="javascript:void(0)" class="closebtn close-nav black"><img src="{{asset('img/back-icon.svg')}}"></a>
                <img src="{{asset('img/logo.svg')}}" class="w-50">
            </div>
            <div class="nav-links">
                <div class="mt-5 py-2 <?php echo $page == 'home' ? 'active-nav' : ''; ?>">
                    <a class="fs-24" href="{{route('home')}}">HOME</a>
                </div>
                <div class="mt-3 py-2 <?php echo $page == 'about' ? 'active-nav' : ''; ?>">
                    <a class="fs-24" href="{{route('about')}}">ABOUT US</a>
                </div>
                <div class="mt-3 py-2 <?php echo $page == 'brands' ? 'active-nav' : ''; ?>">
                    <a class="fs-24" href="{{route('brands')}}">BRANDS</a>
                </div>
                <div class="mt-3 py-2 <?php echo $page == 'terms' ? 'active-nav' : ''; ?>">
                    <a class="fs-24" href="{{route('terms')}}">RENT TERMS</a>
                </div>
                <div class="mt-3 py-2 <?php echo $page == 'faq' ? 'active-nav' : ''; ?>">
                    <a class="fs-24" href="{{route('faq')}}">FAQ</a>
                </div>
                <div class="mt-3 py-2 <?php echo $page == 'blog' ? 'active-nav' : ''; ?>">
                    <a class="fs-24" href="{{route('blog')}}">BLOG</a>
                </div>
                <div class="mt-3 py-2 <?php echo $page == 'contact' ? 'active-nav' : ''; ?>">
                    <a class="fs-24" href="{{route('contact')}}">CONTACT US</a>
                </div>
                <div class="d-flex" style="justify-content: center">
                    <form class="form-inline my-2 my-lg-0 searchForm">
                        <div class="search-input" style="display:none;position: unset">
                            <input class="form-control mr-sm-2 search-car-input" type="search" placeholder="Search" aria-label="Search">
                            <div class="search-result">


                            </div>
                        </div>
                        <a class=" mb-auto mt-1 search-icon" type="submit"><i class="fa fa-search fs-24 text-white mx-3"></i></a>
                    </form>
                </div>
            </div>
            <div class="fs-24 social-nav mt-5  w-100 text-center">
                @foreach($social_item as $item)
                    <a href="{{$item->url}}" target="_blank" class="mx-2"><i class="text-white fa fa-{{$item->icon}}"></i></a>
                @endforeach

            </div>
        </div>

    </div>
</div>

