<?php
//$a=json_decode($brands);
$brandsArray=explode(',',$brands);
$brandsIdArray1=explode(',',$brandsIdArray);
//var_dump($brandsIdArray1);
//exit();
?>
<div class="filter">
    <div class="filter-div">
        <div class="w-100 scroll-menu">
            <div class="mt-5 d-flex justify-content-between">
                <a href="javascript:void(0)" class="close-filter black mx-3"><img src="{{asset('img/back-icon.svg')}}"></a>
                <a href="javascript:void(0)" class=". text-white mx-3">Clear <i class="fa fa-trash "></i></a>

            </div>
            <div class="mt-3">
                <img src="{{asset('img/ad.png')}}" class="w-100">
            </div>
            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-white">Rental Mode</h3>
                    </div>
                    <div class="col-12 mt-2">
                        <input class="form-check-input m-0 rentTypeFilter" type="radio" name="rentType" id="rentTypeDailyFilter"
                               value="1" checked><span class="mx-2 text-white">Per Day</span>
                        <input class="form-check-input m-0 rentTypeFilter" type="radio" name="rentType" id="rentTypeHourlyFilter"
                               value="0"><span class="mx-2 text-white">Per Hour</span>
                    </div>
                    <div class="col-12 mt-2">
                        <div slider id="slider-distance">
                            <div>
                                <div inverse-left style="width:0%;"></div>
                                <div inverse-right style="width:0%;"></div>
                                <div range style="left:0%;right:0%;"></div>
                                <span thumb style="left:0%;"></span>
                                <span thumb style="left:100%;"></span>
                                <div sign style="left:0%;">
                                    <span id="value">0</span>
                                </div>
                                <div sign style="left:100%;">
                                    <span id="value">8000</span>
                                </div>
                            </div>
                            <input type="range" id="lowPrice" tabindex="0" value="0" max="8000" min="0" step="100"/>

                            <input type="range" id="maxPrice" tabindex="0" value="8000" max="8000" min="0" step="100"/>
                        </div>
                        <div class="d-flex justify-content-between text-white">
                            <span>0 $</span>
                            <span>8000 $</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-white">Brand</h3>
                    </div>
                    <div class="col-12 mt-2">

                        @foreach($brandsArray as $brand)
                            <span class="brand <?php echo $brand=="ALL"?'brand-active':''?>" data-brand="{{$brandsIdArray1[$loop->index]}}">
                            {{$brand}}
                        </span>

                        @endforeach

                    </div>
                    <div class="col-12 my-4">
                        <button class="btn btn-primary btn-block filter-btn">
                            APPLY
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
