<?php
$social_item = \App\Models\Social::get();
$contact = \App\Models\Contact::get();
$brands = \App\Models\Brand::get();
?>
<section id="footer" class="mt-5 mx-1 mx-md-5 ">
    <div class="container-fluid  bbw-1 pb-3">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-3 col-md-3 brg-1 text-center text-sm-left">
                <div>
                    <img src="{{asset('img/logo.svg')}}" class="w-75">
                </div>
                <div class="my-5">
                    <div class="mt-5  social-media">
                        @foreach($social_item as $item)
                            <a href="{{$item->url}}" target="_blank" class="text-white mr-2 fs-30"><i
                                    class="text-white fa fa-{{$item->icon}}"></i></a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 col-md-3 brg-1">
                <h4 class="gold bold">TOP BRAND</h4>
                <div class="row">
                    <div class="col-6">
                        <div class="my-4">
                            <?php $i = 1;?>
                            @foreach($brands as $brand)
                                <a class="text-white d-block mt-2"
                                   href="{{route('brand',[$brand->slug])}}">{{$brand->name}}</a>
                                <?php $i++;
                                if ($i == 6) {
                                    echo '   </div>
                    </div>
                    <div class="col-6">
                        <div class="my-4">';
                                }
                                if($i==11){
                                    break;
                                }                                ?>
                            @endforeach


                        </div>
                    </div>
                </div>

            </div>
            <div class="col-12 col-md-6 col-lg-3 col-md-4 brg-1">
                <div class="mt-4">
                    @foreach($contact as $item)
                        <div>
                            <i class="fa fa-{{$item->icon}} text-white fs-24"></i>
                            <p class="d-inline-block ml-3"><a class="gold"
                                                              href="{{$item->url}}">
                                    {{$item->title}}</a></p>
                        </div>
                    @endforeach


                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 col-md-2">
                <img src="{{asset('img/footer.png')}}" class="w-75" style="max-width: 200px">

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <p class="text-white mt-3">2021 ©luxury Rental Car </p>
            </div>
        </div>
    </div>
</section>
</body>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/moment.js')}}"></script>
{{--<script src="{{asset('js/bootstrap.min.js')}}"></script>--}}
<script src="{{asset('js/slick.min.js')}}"></script>
<script src="{{asset('js/intlTelInput.js')}}"></script>
<script src="{{asset('js/daterangepicker.js')}}"></script>
{{--<script src="{{asset('js/wow.js')}}"></script>--}}
<script src="{{asset('js/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/select2.full.min.js')}}"></script>
<script src="{{asset('js/parsley.min.js')}}"></script>
<script type="text/javascript">
    var base_url = @json(url('/'))+'/';
</script>
<script src="{{asset('js/main.js')}}"></script>


</html>
