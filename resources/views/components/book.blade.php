<div class="book-menu">
    <div class="book-div">
        <div class="m-auto scroll-menu">
            <div class="mt-5 d-flex justify-content-between">
                <a href="javascript:void(0)" class="close-book black mx-3"><img
                        src="{{asset('img/back-icon.svg')}}"></a>
                <h5 class="text-white">Your information</h5>
                <a href="javascript:void(0)" class="clear-book  mx-3 gold">Clear</a>

            </div>
            <div class="container-fluid mt-3">
                <form class="mx-2 book-form" data-parsley-validate autocomplete="off">
                    <div class="row">

                        <div class="col-12">
                            <span class="text-white text-center d-block"><i class="fa fa-calendar fs-20 mr-2"></i> PICK DATES</span>
                        </div>
                        <div class="col-12 mt-3 text-center">
                            <input class="form-check-input m-0 rentType" type="radio" name="rentType" id="rentTypeDaily"
                                   value="daily" checked><span class="mx-2 text-white">Daily</span>
                            <input class="form-check-input m-0 rentType" type="radio" name="rentType"
                                   id="rentTypeHourly"
                                   value="hourly"><span class="mx-2 text-white">Hourly</span>
                        </div>
                        <div class="col-12 mt-3">
                            <div>
                                {{--                                <label for="dateTime" class="ml-2 fs-14 mb-0 text-white">Pickup & Drop Off Date*</label>--}}
                                <input class="form-control text-center" id="dateTime" type="text" name="daterange" placeholder="Pickup & Drop Off Date" required/>

                            </div>
                        </div>
                        <div class="col-6 mt-3">

                            <div class="form-group">
                                {{--                                <label for="startTime" class="ml-2 fs-14 mb-0 text-white">Pickup Time*</label>--}}
                                <select required="" class="select2 form-control" id="startTime" data-parsley-errors-container="#startTime-errors">
                                    <option></option>
                                    @for($i=0;$i<=11;$i++)
                                        @for($m=0;$m<=1;$m++)
                                            <option value="<?php echo  $i<10 ?'0'.$i:$i; ?>:<?php echo $m==0 ?'00':'30'; ?>"><?php echo $i!=0?$i<10 ?'0'.$i:$i:'12'; ?>:<?php echo $m==0 ?'00':'30'; ?> <?php echo $i<=11?'AM':'PM'; ?></option>
                                        @endfor
                                    @endfor
                                    @for($i=12;$i<=23;$i++)
                                        @for($m=0;$m<=1;$m++)
                                            <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>:<?php echo $m == 0 ? '00' : '30'; ?>">
                                                <?php if($i>12){
                                                    $x=$i-12;
                                                    if($x>9){
                                                        echo $x;
                                                    }else{
                                                        echo '0'.$x;
                                                    }
                                                }else{
                                                    echo $i;
                                                }?>:<?php echo $m == 0 ? '00' : '30'; ?> <?php echo $i <= 11 ? 'AM' : 'PM'; ?></option>
                                        @endfor
                                    @endfor

                                </select>
                                <div id="startTime-errors"></div>
                            </div>


                        </div>
                        <div class="col-6 mt-3">

                            <div class="form-group">
                                {{--                                <label for="endTime" class="ml-2 fs-14 mb-0 text-white">Drop Off Time*</label>--}}
                                <select required="" class="select2 form-control" id="endTime" data-parsley-errors-container="#endTime-errors">
                                    <option></option>
                                    @for($i=0;$i<=11;$i++)
                                        @for($m=0;$m<=1;$m++)
                                            <option value="<?php echo  $i<10 ?'0'.$i:$i; ?>:<?php echo $m==0 ?'00':'30'; ?>"><?php echo $i!=0?$i<10 ?'0'.$i:$i:'12'; ?>:<?php echo $m==0 ?'00':'30'; ?> <?php echo $i<=11?'AM':'PM'; ?></option>
                                        @endfor
                                    @endfor
                                    @for($i=12;$i<=23;$i++)
                                        @for($m=0;$m<=1;$m++)
                                            <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>:<?php echo $m == 0 ? '00' : '30'; ?>">
                                                <?php if($i>12){
                                                    $x=$i-12;
                                                    if($x>9){
                                                        echo $x;
                                                    }else{
                                                        echo '0'.$x;
                                                    }
                                                }else{
                                                    echo $i;
                                                }?>:<?php echo $m == 0 ? '00' : '30'; ?> <?php echo $i <= 11 ? 'AM' : 'PM'; ?></option>
                                        @endfor
                                    @endfor
                                </select>
                                <div id="endTime-errors"></div>
                            </div>


                        </div>

                        <div class="divider my-4"></div>
                        <div class="col-12 text-white">

                            <div class="form-group">
                                {{--                                <label for="name" class="ml-2 fs-14 mb-0">Name*</label>--}}
                                <input type="text" class="form-control bookInput" id="name" required=""
                                       placeholder="Name">
                            </div>
                            <div class="form-group">
                                {{--                                <label for="phone" class="ml-2 fs-14 mb-0">Phone*</label>--}}
                                <input type="tel" class="form-control bookInput" id="phone" required=""
                                       placeholder="Phone">
                            </div>
                            <div class="form-group">
                                {{--                                <label for="email" class="ml-2 fs-14 mb-0">Email*</label>--}}
                                <input type="email" class="form-control bookInput" id="email" required=""
                                       placeholder="Email">
                            </div>

                            <div class="divider my-4"></div>
                            <div class="row">
                                <div class="col-9 mt-3">
                                    <p class="mx-2 text-white">BABY SET  <span class="gold float-right">25 AED - Per Day</span></p>
                                </div>
                                <div class="col-3 mt-3 d-flex">
                                    <input type="checkbox" id="babySet" class="switch_1 m-auto" value="babySet">
                                </div>
                                <div class="col-9 mt-3">
                                    <p class="mx-2 text-white">ADDITIONAL DRIVER <span class="gold float-right">FREE</span></p>
                                </div>
                                <div class="col-3 mt-3 d-flex">
                                    <input type="checkbox" id="driver" class="switch_1 m-auto" value="driver">
                                </div>
                                <div class="col-9 mt-3">
                                    <span class="mx-2 text-white">POD (Payment On Delivery)</span>
                                </div>
                                <div class="col-3 mt-3 text-center d-flex">

                                    <input class="form-check-input m-auto cashType" type="radio" name="cashType"
                                           checked="checked"
                                           value="POD" id="deliveryBook">
                                </div>
                                <div class="col-9 mt-4">
                                    <span class="mx-2 text-white">Pay Now</span>
                                </div>
                                <div class="col-3 mt-4 text-center d-flex">

                                    <input class="form-check-input m-auto cashType" type="radio" name="cashType"
                                           value="cash" id="paymentBook">
                                </div>
                            </div>
                            <div class="col-12 my-5 px-2 py-3 bg-gray2 br-20">
                                <div class="row">
                                    <div class="col-6">
                                        TOTAL:
                                    </div>

                                    <div class="col-6 text-right bookTotal text-white ">

                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-5 px-1">
                                <button class="btn btn-primary btn-block book-btn" disabled>
                                    CONFIRMATION
                                </button>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>
<div class="confirm-book">
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-12 mt-5 text-center text-white">
                <div class="my-4">
                <span class="fs-20">
                    Reservation Confirmed
                </span>
                </div>
                <div class="">
                    <img src="{{asset('img/check.svg')}}">
                </div>
                <div class="my-5">
                <span class="fs-20">
                    Thank You For Your Trust In
                </span>
                    <h2 class="gold mt-1">
                        LUXURY CAR RENTAL
                    </h2>
                    <span class="fs-20">
                    We Will Contact You Shortly
                </span>
                </div>
                <div class="my-5 pt-5">
                <span class="fs-20">
                    Your Rental Number Is <span class="rental-num"></span>
                </span>
                </div>
            </div>
        </div>
    </div>
</div>
