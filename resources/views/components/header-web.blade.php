<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Luxury Car Rental Dubai">
    <meta property="og:updated_time" content="2021-01-16T09:40:12+00:00">
    @if(!empty($title))
        <meta property="og:title" content="Luxury Car Rental in Dubai | Exotic Car Rental in Dubai, UAE">
    @else
        <meta property="og:title" content="{{$title}}">
    @endif
    @if(!empty($description))
        <meta name="og:description"
              content="{{html_entity_decode($description)}}"/>
        <meta name="description"
              content="{{html_entity_decode($description)}}"/>
    @else
        <meta name="og:description"
              content="Luxury car rental in Dubai (LCR) has experts in car inspections, Who inspect every car before handing it over to the client. Rent your desire car today."/>
        <meta name="description"
              content="Luxury car rental in Dubai (LCR) has experts in car inspections, Who inspect every car before handing it over to the client. Rent your desire car today."/>
    @endif
    @if(!empty($keywords))
        <meta name="keywords" content="{{$keywords}}">
    @else
        <meta name="keywords" content="{{$keywords}}">
    @endif
    <meta name="robots" content="follow,index,max-snippet:-1,max-video-preview:-1,max-image-preview:large"/>
    <meta property="og:url" content="<?php echo url('/');?>">

    <!-- page image -->
    <meta property="og:image:width" content="735">
    <meta property="og:image:height" content="453">
    <meta name="twitter:card" content="summary_large_image">
    @if(!empty($image))
        <meta property="og:image" content="<?php echo url('/').'/';?>{{$image}}">
        <meta property="og:image:url" content="<?php echo url('/').'/';?>{{$image}}">
        <meta property="og:image:secure_url" content="<?php echo url('/').'/';?>{{$image}}">
        <meta name="msapplication-TileImage" content="<?php echo url('/').'/';?>{{$image}}">
    @else
        <meta property="og:image" content="{{asset('img/logo.svg')}}">
        <meta property="og:image:url" content="{{asset('img/logo.svg')}}">
        <meta property="og:image:secure_url" content="{{asset('img/logo.svg')}}">
        <meta name="msapplication-TileImage" content="{{asset('img/logo.svg')}}">
    @endif

<!-- application info -->
    <meta name="apple-mobile-web-app-title" content="Luxury Car Rental in Dubai | Exotic Car Rental in Dubai, UAE">
    <meta name="application-name" content="Luxury Car Rental in Dubai | Exotic Car Rental in Dubai, UAE">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">

    <link rel="icon" href="{{asset('img/logo.svg')}}" type="image/gif"/>
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/intlTelInput.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('css/slick-theme.css')}}" rel="stylesheet">
    {{--    <link href="{{asset('css/animate.css')}}" rel="stylesheet">--}}
    <link href="{{asset('css/sweetalert2.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <title>Luxury Car Rental in Dubai | Exotic Car Rental in Dubai, UAE</title>

    <!-- Fonts  -->

</head>
<body>
<section class="nav-section d-flex py-3 d-block d-md-none">
    <div class="container-fluid m-auto">
        <div class="row">
            <div class="col-3 d-flex">
                <div class="ml-2 my-auto">
                    <i  class="fs-30 open-nav fa fa-bars gold"></i>
                </div>
            </div>
            <div class="col-6 d-flex">
                <div class="text-center m-auto">
                    <a href="{{route('home')}}" class=""> <img src="{{asset('img/logo.svg')}}" class=" w-75"></a>
                </div>
            </div>
            <div class="col-3 ">
                @if($filter=="true")
                    <div class="text-right mr-2">
                        <img src="{{asset('img/fillter.svg')}}" class="w-40 open-filter">
                    </div>
                @endif
                @if($filter=="shear")
                    <div class="text-right mr-2">
                        <a>
                            <img src="{{asset('img/share-icon.svg')}}" class="w-22">
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
