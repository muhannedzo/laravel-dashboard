<?php
return [
    'ProfileImage' => 'صورة الحساب',
    'Name' => 'الأسم',
    'EmailAddress' => 'بريد الألكتروني',
    'Password' => 'كلمة المرور',
];
